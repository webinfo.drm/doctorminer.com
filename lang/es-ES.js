export default async (context, locale) => {
  return await Promise.resolve({
    fullMenu: [
      {
        name: 'Inicio',
        path: '/',
        type: 'internal', // internal, external, void, anchinternalor
        state: false,
        url: null,
        subItems: [],
      },
      {
        name: 'Cree',
        path: '/cree',
        type: 'internal',
        state: false,
        url: null,
        subItems: [],
      },
      {
        name: 'Aprende',
        path: '/aprende',
        type: 'void',
        state: false,
        url: null,
        subItems: [
          {
            name: 'Asesorías',
            path: '/aprende/mentor',
            type: 'internal',
            state: false,
            url: null,
            subItems: [],
          },
          {
            name: 'Entrevistas',
            path: '/aprende/entrevistas',
            type: 'internal',
            state: false,
            url: null,
            subItems: [],
          },
          {
            name: 'Podcast',
            path: '/aprende/podcast',
            type: 'internal',
            state: false,
            url: null,
            subItems: [],
          },
        ],
      },
      {
        name: 'Mina',
        path: '/mina',
        type: 'void',
        state: false,
        url: null,
        subItems: [
          {
            name: 'Productos',
            path: '/mina/productos',
            type: 'void',
            state: false,
            url: null,
            subItems: [
              {
                name: "ASIC'S",
                path: '/mina/productos/asics',
                type: 'internal',
                state: false,
                url: null,
                subItems: [],
              },
              {
                name: 'Hashportable',
                path: '/mina/productos/hashportable',
                type: 'internal',
                state: false,
                url: null,
                subItems: [],
              },
            ],
          },
          {
            name: 'Servicios',
            path: '/mina/servicios',
            type: 'void',
            state: false,
            url: null,
            subItems: [
              {
                name: 'Tribu',
                path: '/mina/servicios/tribu',
                type: 'internal',
                state: false,
                url: null,
                subItems: [],
              },
              {
                name: 'Hosting',
                path: '/mina/servicios/hosting',
                type: 'internal',
                state: false,
                url: null,
                subItems: [],
              },
              {
                name: 'Infraestructura',
                path: '/mina/servicios/infraestructura',
                type: 'internal',
                state: false,
                url: null,
                subItems: [],
              },
            ],
          },
          {
            name: 'Recursos',
            path: '/mina/recursos',
            type: 'internal',
            state: false,
            url: null,
            subItems: [],
          },
        ],
      },
      {
        name: 'FAQS',
        path: '/faqs',
        type: 'internal',
        state: false,
        url: null,
        subItems: [],
      },
      {
        name: 'Nosotros',
        path: '/nosotros',
        type: 'internal',
        state: false,
        url: null,
        subItems: [],
      },
      {
        name: 'Contacto',
        path: '/contacto',
        type: 'internal',
        state: false,
        url: null,
        subItems: [],
      },
    ],
    contact: {
      form: {
        fields: {
          name: 'Nombre',
          lastname: 'Apellido',
          email: 'Correo electrónico',
          phone: 'Número telefónico',
          message: 'Mensaje',
        },
        button: 'Enviar',
        title: 'Contáctanes',
        messages: {
          success:
            'Su correo ha sido enviado exitosamente, pronto nos estaremos contactando con usted. Gracias.',
          error:
            'Hubo un error al enviar su correo. Por favor intentelo más tarde o contáctenos vía WhatsApp.',
        },
      },
    },
    footer: {
      nav: {
        us: {
          title: 'Nosotros',
          links: ['Storytelling', 'Personal'],
        },
        believe: {
          title: 'Cree',
          links: ['Propósito', 'Manifesto'],
        },
        learn: {
          title: 'Aprende',
          links: ['Asesorías', 'Entrevistas', 'Podcast'],
        },
        mine: {
          title: 'Mina',
          links: [
            'Asics',
            'Tribu',
            'Hosting',
            'Infrastructura',
            'Hashportables',
            'Rcursos',
          ],
        },
        follow: {
          title: 'Síguenos',
        },
      },
      copyright: 'Todos los derechos reservados',
    },
    calculator: {
      title: 'Calculadora',
      form: {
        fields: {
          hashrate: 'Hashrate',
          consumption: 'Consumo energético',
          opex: 'Costos operativo (OPEX)',
          cost: 'Costo de la máquina (USD)',
          hashprice: 'Hashprice',
        },
      },
      results: {
        title: 'Proyecciones',
        fields: {
          'daily-income': 'Ingresos diarios',
          'daily-profit': 'Ganancia diaria',
          'monthly-profit': 'Ganancia mensual',
          'roi-time': 'Tiempo de ROI',
          'annual-roi': 'ROI anual',
        },
        button: 'Compartir',
      },
    },
    believe: {
      title: 'Cree',
      purpose: {
        title: 'Propósito',
        text: 'Sembrar los recursos de Latinoamérica para contribuir a su desarrollo e impulsar el crecimiento y la libertad de las personas.',
      },
      manifest: {
        title: 'Manifiesto',
        text: `Minar es una forma de pensar.
        Es una actitud que asumimos para la vida.
        Es una postura firme para participar en algo grande.
        Para abrazar una idea que lo cambió todo.
        
        Minar es una forma de creer.
        Y nosotros creemos en la descentralización.
        Por eso minamos Bitcoin.
        Porque en Bitcoin no hay dioses ni dueños.
        
        Minar es una forma de actuar.
        Es avanzar con propósito, visión y estrategia.
        Es hacer siempre el uso más eficiente de los recursos.
        Para transformar la energía en valor, 24/7.
        
        Minar es una oportunidad para el progreso.
        Es una solución para la industria energética.
        Es un motor para el crecimiento de Latinoamérica.
        Es una fuente de libertad para las personas.
        
        Minar es un símbolo de resistencia.
        Nos llamarán románticos por ser apasionados.
        Nos llamarán raros por ser disruptivos.
        Nos dirán locos, pero la única locura sería no hacerlo.`,
      },
    },
    us: {
      text: `Somos una empresa de minería creyente de Bitcoin que potencia la economía financiera descentralizada desde Latinoamérica.

      Creamos una plataforma de ideas, servicios y recursos para el desarrollo eficiente y placentero de la minería y te invitamos a nuestro movimiento de minería en español que inspira a las personas a creer, aprender y minar Bitcoin para mejorar su vida.`,
      staff: {
        title: 'Staff',
        positions: [
          'CEO',
          'CBO',
          'Gerente de Operaciones',
          'Gerente de Finanzas',
          'Coordinador de Ventas',
          'Coordinador de Operaciones',
          'Líder de Servicio Técnico Barquisimeto',
          'Líder de Servicio Técnico Caracas',
          'Arquitecto',
        ],
      },
    },
    faqs: {
      categoriesTitle: 'Categorías',
      categories: ['ventas', 'hospedaje', 'tribu', 'legal', 'otros'],
      questions: {
        ventas: [
          {
            question: '¿Tienen máquinas de minería a la venta?',
            content: `Si, nos puedes contactar a nuestro número corporativo para obtener más información sobre los modelos disponibles y nuestras ofertas.`,
          },
          {
            question: '¿Qué máquinas de minería venden?',
            content: `Vendemos cualquier generación, modelo y marca de máquinas de minería usadas y nuevas. Si estás interesado contáctanos a través de número de whatsapp.`,
          },
          {
            question: '¿Qué modelos de fuentes de poder tienen disponibles?',
            content: `Ofrecemos todo tipo de fuentes de poder para máquinas de minería dependiendo de nuestra disponibilidad en inventario. Si no tenemos existencia en inventario, ofrecemos la posibilidad de realizar el requerimiento bajo pedido. Si deseas más información escríbenos a través de nuestro número corporativo.`,
          },
          {
            question: '¿Venden repuestos para máquinas de minería?',
            content: `Si, tenemos ofertas limitadas de distintos componentes de gran variedad de modelos de máquinas de minería.  ofrecemos la posibilidad de realizar el requerimiento bajo pedido. Para más información escríbenos por Whatsapp Business. `,
          },
          {
            question: '¿Venden máquinas de minería a personas naturales?',
            content: `Si, la persona natural debe presentar un contrato con alguna empresa de minería nacional que cuente con las licencias y permisos respectivos. Sin embargo, Doctorminer ofrece el servicio de hospedaje a partir de cierta cantidad de equipos, hashrate y consumo.`,
          },
          {
            question:
              '¿Tienen la licencia de comercialización emitida por la Superintendencia Nacional de Criptoactivos (SUNACRIP)? ',
            content: `Si, contamos con la licencia de comercialización emitida por la Superintendencia Nacional de Criptoactivos (SUNACRIP), esto nos permite vender equipos de minería digital a nivel nacional.`,
          },
          {
            question:
              '¿Cuántos días de garantía ofrecen por la compra de una máquina de minería? ',
            content: `Los días de garantía varían dependiendo de la condición del equipo (nuevo o usado), generación y modelo, sin embargo nuestro estándar es de 21 días de garantía sobre equipos usados.`,
          },
          {
            question: 'Contacto de Ventas',
            content: `
<a
  href="https://api.whatsapp.com/send?phone=582123241954"
  class="flex items-center justify-start font-semibold hover:text-primary hover:underline"
>
  <svg
    xmlns="http://www.w3.org/2000/svg"
    fill="currentColor"
    viewBox="0 0 24 24"
    class="w-5 h-5 mr-2"
  >
    <path
      fill-rule="evenodd"
      clip-rule="evenodd"
      d="M18.403 5.633A8.919 8.919 0 0 0 12.053 3c-4.948 0-8.976 4.027-8.978 8.977 0 1.582.413 3.126 1.198 4.488L3 21.116l4.759-1.249a8.981 8.981 0 0 0 4.29 1.093h.004c4.947 0 8.975-4.027 8.977-8.977a8.926 8.926 0 0 0-2.627-6.35m-6.35 13.812h-.003a7.446 7.446 0 0 1-3.798-1.041l-.272-.162-2.824.741.753-2.753-.177-.282a7.448 7.448 0 0 1-1.141-3.971c.002-4.114 3.349-7.461 7.465-7.461a7.413 7.413 0 0 1 5.275 2.188 7.42 7.42 0 0 1 2.183 5.279c-.002 4.114-3.349 7.462-7.461 7.462m4.093-5.589c-.225-.113-1.327-.655-1.533-.73-.205-.075-.354-.112-.504.112s-.58.729-.711.879-.262.168-.486.056-.947-.349-1.804-1.113c-.667-.595-1.117-1.329-1.248-1.554s-.014-.346.099-.458c.101-.1.224-.262.336-.393.112-.131.149-.224.224-.374s.038-.281-.019-.393c-.056-.113-.505-1.217-.692-1.666-.181-.435-.366-.377-.504-.383a9.65 9.65 0 0 0-.429-.008.826.826 0 0 0-.599.28c-.206.225-.785.767-.785 1.871s.804 2.171.916 2.321c.112.15 1.582 2.415 3.832 3.387.536.231.954.369 1.279.473.537.171 1.026.146 1.413.089.431-.064 1.327-.542 1.514-1.066.187-.524.187-.973.131-1.067-.056-.094-.207-.151-.43-.263"
    ></path>
  </svg>
  <span>+58 212-3241954</span>
</a>
            `,
          },
        ],
        hospedaje: [
          {
            question: '¿Dónde prestan el servicio de hospedaje?',
            content: `Nuestra empresa está en la región latinoamericana, específicamente en Venezuela. Por temas de seguridad no revelamos las ubicaciones de nuestras infraestructuras de hashrate.`,
          },
          {
            question: '¿Cuál es el costo del servicio?',
            content: `
<p>Tenemos 2 modelos de negocio que giran en función al <b>riesgo de volatilidad</b> causado por la fluctuación constante de la dificultad de la red y el precio de BTC, que afectan directamente la estabilidad en la producción de los ASICS:</p>
<ol class="leading-6 list-decimal list-inside list">
  <li><b>Costo Fijo:</b> Donde el cliente absorbe todo del <b>riesgo de volatilidad</b>, al pagar a Doctorminer un monto constante correspondiente al consumo eléctrico de sus equipos de minería (KWh)</li>
  <li><b>Costos variable:</b> Donde el cliente <b>mitiga parte del riesgo de volatilidad</b>, al compartir con Doctorminer un % de los ingresos o las ganancias de sus equipos.</li>
</ol>
`,
          },
          {
            question: '¿Qué beneficios tiene?',
            content: `
<ul class="leading-6 list-disc list-inside list">
  <li>Ahorro de dinero y tiempo en la construcción de la infraestructura.</li>
  <li>Ahorro de tiempo en la gestión de los permisos legales de minería y factibilidad energética.</li>
  <li>Uptime mayor al 85%</li>
  <li>Control y monitoreo 24/7 de tus equipos de minería.</li>
  <li>Atención personalizada</li>
</ul>
`,
          },
          {
            question: '¿Qué incluye el servicio?',
            content: `
<ul class="leading-6 list-disc list-inside list">
  <li>Conexión de los equipos al mining pool TRIBU.</li>
  <li>Servicio técnico de tus equipos de minería (ver ficha)</li>
  <li>Todos las recompensas por minería serán depositados a la wallet designada por el cliente y a final de mes se envía una notificación de cobro por el servicio prestado (servicio pospago).</li>
  <li>Contrato de servicio en el que se expresan las responsabilidades del cliente y Doctorminer.</li>
  <li>Acceso en modalidad remota a un link de visualización del mining pool y al panel de control del usuario.</li>
  <li>Software de control y monitoreo.</li>
  <li>Informes mensuales.</li>
</ul>
`,
          },
          {
            question: '¿Qué hacer para contratar el servicio?',
            content: `
<ul class="leading-6 list-disc list-inside list">
  <li>Notificar cantidad y modelo de máquinas a hospedar.</li>
  <li>Consultar disponibilidad de carga energética y espacios.</li>
  <li>Solicitar a Doctorminer propuesta comercial.</li>
  <li>Confirmar ubicación geográfica de los equipos de minería</li>
  <li>Firmar contrato del servicio. </li>
</ul>
`,
          },
          {
            question:
              '¿Qué máquinas puedo hospedar en el servicio de Doctorminer?',
            content: `<p>Hospedamos equipos con el algoritmo SHA-256 de Bitcoin. Tales como:</p>
<ul class="leading-6 list-disc list-inside list">
  <li>Antminer S9, S11, S15 y S19 </li>
  <li>Antminer T15, T17, T19</li>
  <li>Antminer L3 y L7</li>
  <li>Antminer Z11 y Z15</li>
  <li>Whatsminer M20s, M21s, M30s y M31s</li>
  <li>Avalonminer 841, 1046, 1126 y 1166</li>
</ul>
`,
          },
          {
            question: 'Contacto de Ventas',
            content: `
<a
  href="https://api.whatsapp.com/send?phone=582123241954"
  class="flex items-center justify-start font-semibold hover:text-primary hover:underline"
>
  <svg
    xmlns="http://www.w3.org/2000/svg"
    fill="currentColor"
    viewBox="0 0 24 24"
    class="w-5 h-5 mr-2"
  >
    <path
      fill-rule="evenodd"
      clip-rule="evenodd"
      d="M18.403 5.633A8.919 8.919 0 0 0 12.053 3c-4.948 0-8.976 4.027-8.978 8.977 0 1.582.413 3.126 1.198 4.488L3 21.116l4.759-1.249a8.981 8.981 0 0 0 4.29 1.093h.004c4.947 0 8.975-4.027 8.977-8.977a8.926 8.926 0 0 0-2.627-6.35m-6.35 13.812h-.003a7.446 7.446 0 0 1-3.798-1.041l-.272-.162-2.824.741.753-2.753-.177-.282a7.448 7.448 0 0 1-1.141-3.971c.002-4.114 3.349-7.461 7.465-7.461a7.413 7.413 0 0 1 5.275 2.188 7.42 7.42 0 0 1 2.183 5.279c-.002 4.114-3.349 7.462-7.461 7.462m4.093-5.589c-.225-.113-1.327-.655-1.533-.73-.205-.075-.354-.112-.504.112s-.58.729-.711.879-.262.168-.486.056-.947-.349-1.804-1.113c-.667-.595-1.117-1.329-1.248-1.554s-.014-.346.099-.458c.101-.1.224-.262.336-.393.112-.131.149-.224.224-.374s.038-.281-.019-.393c-.056-.113-.505-1.217-.692-1.666-.181-.435-.366-.377-.504-.383a9.65 9.65 0 0 0-.429-.008.826.826 0 0 0-.599.28c-.206.225-.785.767-.785 1.871s.804 2.171.916 2.321c.112.15 1.582 2.415 3.832 3.387.536.231.954.369 1.279.473.537.171 1.026.146 1.413.089.431-.064 1.327-.542 1.514-1.066.187-.524.187-.973.131-1.067-.056-.094-.207-.151-.43-.263"
    ></path>
  </svg>
  <span>+58 212-3241954</span>
</a>
            `,
          },
        ],
        tribu: [
          {
            question: '¿Qué es un mining pool? ',
            content: `Un pool de minería es una plataforma tecnológica que permite la integración del poder de cómputo creado por varios mineros con el fin de aumentar la seguridad de la red, la descentralización y la eficiencia financiera de la actividad.`,
          },
          {
            question: '¿Cuánto produce mi máquina en Tribu? ',
            content: `
<p>Dependiendo del poder de minado y de la eficiencia de conectividad de tu máquina, en un mes es que podrás verificar realmente cuánto produce una máquina de minería en Tribu Pool o en cualquier mining pool del mundo. Sin embargo, existen herramientas que te ayudan a proyectar tus ingresos, con base en tu hashrate, el consumo de tu equipo de minería y costo eléctrico,  el porcentaje que le pagas al mining pool y la eficiencia de tu operación de minería. </p>
<a href="https://hashrateindex.com/tools/calculator" target="_blank">https://hashrateindex.com/tools/calculator</a>
`,
          },
          {
            question:
              '¿Dónde encuentro el aproximado de lo que lleva Tribu acumulado? ¿En qué parte/sección del pool está? paso a paso ',
            content: `
<p>Para saber el acumulado que lleva el Pool, sigue estos pasos: </p>
<ul class="leading-6 list-decimal list-inside list">
  <li>Inicia sesión con tu cuenta en el pool,</li>
  <li>Una vez iniciada la sesión, debes dirigirte al menú que se ubica en el lado superior izquierdo.</li>
  <li>En el menú buscarás la sección de <b>Mining Overview / Resumen de Minería.</b></li>
  <li>Una vez dentro de la sección de <b>Mining Overview / Resumen de minería</b>, aparecerán dos opciones en el panel principal.</li>
  <li>Luego de haber seleccionado la pestaña <b>POOL</b>, allí tendrás el <b>valor del acumulado de TRIBU Pool</b>; así como también tendrás otro tipo de información que te pueden ayudar más adelante.</li>
</ul>
<br>
<p>En la pestaña POOL, también podrás encontrar: </p>
<ul class="leading-6 list-disc list-inside list">
  <li>Hashrate promedio del pool en la última hora,</li>
  <li>Promedio por PH (Petahash) por día,</li>
  <li>Comisión del Pool.</li>
</ul>
<br>
<p class="text-xs">En cada uno de los renglones, podrás visualizar la i de información, el mismo posee la leyenda de cada uno</p>
`,
          },
          {
            question: '¿Cuál es la modalidad de pago de Tribu? ',
            content: `FPPS (FULL-PAY-PER-SHARE). Esta modalidad es la más beneficiosa para los mineros, ya que el pool asume todos los riesgos de la variación en las recompensas del bloque (suerte), esto aplica para ambas partes de la recompensa, tanto la que viene de las comisiones de las transacciones como de los nuevos BTC creados.`,
          },
          {
            question:
              '¿Cuánto es el costo del FEE de transferencia a mi wallet? ¿Se transfiere automáticamente?',
            content: `Nuestras comisiones de pago tienden a ser cero, ya que hacemos un solo pago para todos los mineros, es decir, el costo de la transacción se divide entre todos los mineros, un outputs para muchos inputs. Las ganancias obtenidas se depositan automáticamente en la wallet, dependiendo del intervalo para los pagos que ha sido configurado en el pool.`,
          },
          {
            question:
              '¿Qué diferencia tiene minar con Tribu que minar con otro mining pool?',
            content: `
<ul class="leading-6 list-disc list-inside list">
  <li>Atención al cliente en español (idioma local).</li>
  <li>Sistema de pagos FPPS que mitiga riesgos de los mineros</li>
  <li>Servidores geográficamente ubicados en la misma región, permitiendo baja latencia de conexión.</li>
  <li>Manual de usuario.</li>
  <li>Cobro de comisiones de forma transparente.</li>
  <li>Sistema de subcuentas para un mismo usuario.</li>
  <li>Umbral de pagos flexible, desde 0.001 BTC a 0.5 BTC le permite funcionar para mineros de todas las escalas. </li>
  <li>Comisiones por retiros que tienden a ser 0 al ser diluido entre todos los usuarios de TRIBU.</li>
  <li>Configuración de alertas por fluctuaciones de hashrate.</li>
</ul>
`,
          },
          {
            question: '¿Se puede configurar un equipo con BraiinsOS a TRIBU? ',
            content: `
<p>Si, se puede configurar cualquier equipo que mine con el algoritmo SHA-256. De todas maneras te invitamos a ver el siguiente video donde te indicamos cómo configurar el equipo</p>
<br>
<a href="https://t.me/doctorminerVE/13720" target="_blank" class="btn btn-primary">Video</a>
`,
          },
          {
            question: '¿Cuál es el umbral de pago de TRIBU?',
            content: `El umbral de pago es flexible y lo puedes configurar en un valor que se encuentre entre 0.001 - 0.5 BTC. La comisión de retiro hacia tu wallet tiende a cero.`,
          },
          {
            question: '¿Cómo me puedo unir a TRIBU?',
            content: `
<ul class="leading-6 list-disc list-inside list">
  <li>Ingresa a google en tu buscador colocando <a href="https://mining.doctorminer.com/login" target="_blank">https://mining.doctorminer.com/login</a></li>
  <li>Una vez que estés dentro de la página debes registrarte con tu correo electrónico de preferencia y contraseña (clave con letras y números para aumentar la seguridad). Debes confirmar tu contraseña para finalizar tu registro.</li>
  <li>Elige un nombre de usuario, te llegará un correo electrónico para verificar tu cuenta. En caso tal de que el nombre de usuario ya esté registrado debes probar con otro diferente.</li>
  <li>Al verificar tu cuenta se abrirá directamente una pestaña nueva comprobando tu correo. En el siguiente formulario ingresa tu correo y contraseña.</li>
</ul>
            `,
          },
          {
            question: '¿Cuál es el stratum para configurar los equipos?',
            content: `Para configurar  tu miner a nuestro pool debes asociarlo con el nombre de usuario con el que te registraste y utilizando nuestro stratum: <b><a href="bitcoin.doctorminer.com:700" target="_blank">bitcoin.doctorminer.com:700</a></b>

<ul class="leading-6 list-disc list-inside list">
  <li>Configura tu Worker: Username/WorkerName</li>
  <li>Configura tu Password: x</li>
</ul>
<br>
El username puede ser un account o sub account.
<br>
Para más información acerca de TRIBU te invitamos a unirte a nuestro canal de Telegram: <a href="https://t.me/doctorminerpool" target="_blank">https://t.me/doctorminerpool</a>
`,
          },
          {
            question: '¿Cuáles son los requisitos para entrar en Tribu?',
            content: `Sólo con tener un equipo de minería del algoritmo SHA-256 de la moneda de Bitcoin, puedes entrar a nuestro pool.`,
          },
          {
            question: '¿Qué criptomonedas opera Tribu?',
            content: `Bitcoin y Ethereum, algoritmo Sha-256 y ETHash`,
          },
          {
            question: '',
            content: `
<ul class="leading-6 list-disc list-inside list">
  <li>Atención al cliente en español (idioma local).</li>
  <li>Sistema de pagos FPPS que mitiga riesgos de los mineros</li>
  <li>Servidores geográficamente ubicados en la misma región, permitiendo baja latencia de conexión.</li>
  <li>Manual de usuario.</li>
  <li>Cobro de comisiones de forma transparente.</li>
  <li>Sistema de subcuentas para un mismo usuario.</li>
  <li>Umbral de pagos flexible, desde 0.001 BTC a 0.5 BTC le permite funcionar para mineros de todas las escalas. </li>
  <li>Comisiones por retiros que tienden a ser 0 al ser diluido entre todos los usuarios de TRIBU.</li>
  <li>Configuración de alertas por fluctuaciones de hashrate.</li>
</ul>
`,
          },
          {
            question: '¿Cuáles son las desventajas?',
            content: `
<ul class="leading-6 list-disc list-inside list">
  <li>La nueva versión demora un poco más en la detección de los equipos de minería cuando empiezan a levantar hashrate por primera vez o cuando cambian de un estado Active a Warning o Inactive. </li>
  <li>No contamos con una app, sin embargo, tenemos una versión móvil que funciona muy bien. </li>
</ul>`,
          },
          {
            question: '¿Qué wallet debo usar? ',
            content: `Recomendamos usar wallets non custodials o carteras frías. Tribu admite cualquier tipo de billetera de Bitcoin válida. `,
          },
          {
            question: '¿El umbral de pago se configura para cada subcuenta? ',
            content: `Si, una vez que te registres en TRIBU Pool, puedes configurar varias subcuentas. En cada una de ellas puedes configurar wallets de pago distintas y cada umbral de pago configurado es independiente de cada subcuenta.`,
          },
          {
            question:
              '¿Qué significa la señalización verde, amarilla y roja en la sección "mineros"?',
            content: `En la siguiente imagen podemos observar 3 iconos. Los cuales denotan:
<ul class="leading-6 list-disc list-inside list">
  <li><b>✔  Verde (activas):</b> las máquinas conectadas y minando.</li>
  <li><b>✔  Amarillo (alertas):</b> este icono, indica las maquinas alarmadas, que han presentado algún reinicio o falla breve de conexión al pool.</li>
  <li><b>✔  Rojo (inactiva):</b> son máquinas que han perdido conexión con el pool por alrededor de 1 hora.</li>
</ul>
`,
          },
          {
            question: '¿Cuál es el gráfico de hashrate total (global).',
            content: `Mediante el siguiente gráfico, podemos ver el rendimiento de las máquinas en escalas comprendidas entre 15 minutos a 24 horas.`,
          },
          {
            question: '¿ Cuál es la Tabla resumen del mining pool?',
            content: `Las imágenes anteriores muestran la tabla resumen para la subaccount que se ha seleccionado, en la cual podemos observar el hashrate y la eficiencia total, al igual que los ingresos minados. Se puede seleccionar los intervalos entre 15 minutos a 24 horas.`,
          },
          {
            question: 'Búsqueda por nombre.',
            content: `Con ayuda de la siguiente barra, puedes buscar algún minero de tu interés. Solo debemos escribir el nombre con que se ha configurado el equipo en la barra para iniciar la búsqueda.`,
          },
          {
            question: 'Selección de organización.',
            content: `Mediante estas listas, se puede establecer un orden de visualización de los workers, según sea la selección realizada.`,
          },
          {
            question:
              '¿Cómo hago para formar parte del programa de referidos y qué beneficios obtengo? ',
            content: `Si deseas formar parte del programa de referidos debes dirigirte a la última opción del menú que dice "<b>referidos</b>".
<br>
Para generar tu enlace, haz click en "<b>referidos</b>", deberás bajar hasta la opción "<b>lista de enlace de referencia</b>".
<br>
Pulsa la opción "<b>generar enlace</b>", coloca el código referencial que desees. Una vez creado podrás copiar el enlace y hacerlo llegar a las personas que quieras referir.
<br>
Si quieres monitorear la ganancia obtenida por tus referidos, dirígete al apartado de la pantalla donde dice "<b>ganancias diarias</b>".
<br>
Los <b>beneficios</b> que puedes obtener son del 10% del fee del minero que la persona refiere. ( es el 10% del 2.25% del fee).  
`,
          },
          {
            question: '¿Se puede dividir en varias carteras los ingresos? ',
            content: `
Para añadir tu cartera a Tribu, debes dirigirte a la segunda sección del menú que dice "<b>Subaccount Management</b>" y haz click en "<b>Cartera</b>".
<br>
Pulsa la opción "<b>añadir cartera</b>", seguidamente "<b>crear nueva opción de cartera</b>".
<br>
Luego, tendrás el siguiente cuadro "<b>crear nueva dirección de cartera de BTC</b>". Allí colocarás el nombre de tu cartera y la dirección de la misma. 
<br>
Una vez ingresados los datos, debes hacer click en "<b>crear dirección de cartera</b>".
`,
          },
        ],
        legal: [
          {
            question:
              '¿Cuál es el marco regulatorio para la industria de minería de Bitcoin en Venezuela? ',
            content: `
<b>GACETAS:</b>
<div class="flex flex-col">
<div class="mt-5 w-full max-w-[320px] mx-auto lg:max-w-full overflow-x-auto shadow">
  <div class="inline-block min-w-full py-2 align-middle">
    <table id="gacetas-table" class="min-w-full border divide-y border-primary divide-primary">
    <thead class="bg-transparent border border-primary">
      <tr>
        <th scope="col" class="p-3 text-sm font-semibold text-left border text-primary border-primary">Acto normativo</th>
        <th scope="col" class="p-3 text-sm font-semibold text-left border text-primary border-primary">Nº Gaceta Oficial</th>
        <th scope="col" class="p-3 text-sm font-semibold text-left border text-primary border-primary">Fecha</th>
        <th scope="col" class="p-3 text-sm font-semibold text-left border text-primary border-primary">Vigencia</th>
        <th scope="col" class="p-3 text-sm font-semibold text-left border text-primary border-primary">Contenido</th>
        <th scope="col" class="p-3 text-sm font-semibold text-left border text-primary border-primary">Autoridad Pública</th>
      </tr>
    </thead>
    <tbody class="bg-transparent divide-y divide-primary">
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decreto n° 3.196</span></td>
        <td class="tablet-col">6.346 extraordinario</td>
        <td class="tablet-col">8 de Diciembre de 2017</td>
        <td class="tablet-col">Derogada</td>
        <td class="tablet-col">Se autoriza la creación de la SUPCAVEN</td>
        <td class="tablet-col">Presidencia de la República</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decreto n° 3.292</span></td>
        <td class="tablet-col">41.347</td>
        <td class="tablet-col">23 de Febrero de 2018</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Se determina como respaldo para la implementación de operaciones de intercambio financiero y comercial a través de Criptoactivos.</td>
        <td class="tablet-col">Presidencia de la República</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decreto n° 3.333</span></td>
        <td class="tablet-col">41.366</td>
        <td class="tablet-col">22 de marzo de 2018</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Creación Zonas Petro</td>
        <td class="tablet-col">Presidencia de la República</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decreto Constituyente sobre Criptoactivos y Moneda Soberana Petro</span></td>
        <td class="tablet-col">6.370 extraordinario</td>
        <td class="tablet-col">9 de abril de 2018</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Decreto constituyente sobre criptoactivos y la Criptomoneda Soberana Petro</td>
        <td class="tablet-col">Asamblea Nacional Constituyente</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decreto n° 3.353</span></td>
        <td class="tablet-col">6.371 extraordinario</td>
        <td class="tablet-col">9 de abril de 2018</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Creación de Tesorería de Criptoactivos S.A.</td>
        <td class="tablet-col">Presidencia de la República</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decreto n° 3.355</span></td>
        <td class="tablet-col">6.371 extraordinario</td>
        <td class="tablet-col">9 de abril de 2018</td>
        <td class="tablet-col">Derogada</td>
        <td class="tablet-col">Creación de SUPCACVEN</td>
        <td class="tablet-col">Presidencia de la República</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decreto n° 3.470</span></td>
        <td class="tablet-col">41.422</td>
        <td class="tablet-col">19 de Junio de 2018</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Se adscribe al Ministerio del Poder Popular de Industrias y Producción Nacional a la SUPCAVEN</td>
        <td class="tablet-col">Presidencia de la República</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decreto n°. 3.719</span></td>
        <td class="tablet-col">6.420 extraordinario</td>
        <td class="tablet-col">28 de Diciembre de 2018</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Los sujetos pasivos que realicen operaciones en el territorio nacional en moneda extranjera o criptodivisas, autorizadas por la Ley, deben determinar y pagar las obligaciones en moneda extranjera o criptodivisas</td>
        <td class="tablet-col">Presidencia de la República</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decreto Constituyente sobre Sistema Integral de Criptoactivos</span></td>
        <td class="tablet-col">41.575</td>
        <td class="tablet-col">30 de Enero de 2019</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Sistema Integral de Criptoactivos (se cambia denominación de SUPCAVEN a SUNACRIP)</td>
        <td class="tablet-col">Asamblea Nacional Constituyente</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decreto n° 4.025</span></td>
        <td class="tablet-col">41.763</td>
        <td class="tablet-col">19 de Noviembre de 2019</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Se instruye a las personas naturales y jurídicas, públicas y privadas en cuanto a la obligatoriedad del registro de información y hechos económicos expresados contablemente en Criptoactivos Soberanos, sin perjuicio de su registro en bolívares, según corresponda.</td>
        <td class="tablet-col">Presidencia de la República</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Providencia 097-2019</span></td>
        <td class="tablet-col">41.782</td>
        <td class="tablet-col">16 de Diciembre de 2019</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Se establecen los parámetros aplicables a las operaciones de asignación e intercambio de recursos con criptoactivos a través de plataformas tecnológicas altamente especializadas, que garanticen la transparencia, seguridad y trazabilidad de las operaciones.</td>
        <td class="tablet-col">SUNACRIP</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Providencia 098-2019</span></td>
        <td class="tablet-col">41.787</td>
        <td class="tablet-col">26 de Diciembre de 2019</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Se establece los parámetros para la presentación de la información financiera, reexpresión de los registros contables de operaciones y hechos económicos con Criptoactivos, realizadas por las personas naturales que se encuentren obligadas a llevar registros contables, así como las personas jurídicas, públicas y privadas, en el territorio de la República Bolivariana de Venezuela.</td>
        <td class="tablet-col">SUNACRIP</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decreto n° 4.083</span></td>
        <td class="tablet-col">41.789</td>
        <td class="tablet-col">27 de Diciembre de 2019</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Se crean como Zonas Económicas Especiales del Criptoactivo Soberano (Petro), el área minera número 2, denominada “Manuelita Sáenz”</td>
        <td class="tablet-col">Presidencia de la República</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decreto n°. 4.096 extraordinario</span></td>
        <td class="tablet-col">41.800</td>
        <td class="tablet-col">15 de Enero de 2020</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Se procede a la liquidación, venta y pago de servicios en Criptoactivos Soberanos Petro (PTR)</td>
        <td class="tablet-col">Presidencia de la República</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Providencia 057-2020</span></td>
        <td class="tablet-col">41.955</td>
        <td class="tablet-col">1 de Septiembre de 2020</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Se establecen los requisitos y trámites para el envío y recepción de remesas en Criptoactivos a personas naturales y jurídicas en el territorio de la República Bolivariana de Venezuela</td>
        <td class="tablet-col">SUNACRIP</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Providencia 084-2020</span></td>
        <td class="tablet-col">41.969</td>
        <td class="tablet-col">21 de Septiembre de 2020</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Se regula las actividades relacionadas con el uso, importación, comercialización de equipos de Minería Digital, partes y piezas de éstos, equipamiento y acondicionamiento de espacios para ofrecer el servicio de hospedaje a equipos de Minería Digital, incluida la fabricación, ensamblaje, reparación y mejoras de tales equipos, así como que provean el servicio de minera digital en la nube.</td>
        <td class="tablet-col">SUNACRIP</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Providencia 044-2021</span></td>
        <td class="tablet-col">42.110</td>
        <td class="tablet-col">21 de Abril de 2021</td>
        <td class="tablet-col">Vigente</td>
        <td class="tablet-col">Se dictan las Normas relativas a la administración y fiscalización de los riesgos relacionados con la legitimación de capitales, el financiamiento del terrorismo y el financiamiento de la proliferación de armas de destrucción masiva, aplicables a los proveedores de servicios de activos virtuales y a las personas y entidades que proporcionen productos y servicios a través de actividades que involucren activos virtuales, en el Sistema Integral de Criptoactivos.</td>
        <td class="tablet-col">SUNACRIP</td>
      </tr>
    </tbody>
  </table>
  </div>
</div>
</div>

`,
          },
        ],
        otros: [
          {
            question: '¿Qué me recomiendas para comenzar a minar ?',
            content: `Te invitamos a iniciar nuestra asesoría "mi primera operación de minería"`,
          },
        ],
      },
    },
    consulting: {
      title: 'Tópicos',
      topics: [
        {
          topic: 'Mentor',
          content: `Nos inspira llevar adelante un negocio con impacto positivo, cálido, cercano y creativo. Queremos transmitir nuestra convicción en Bitcoin y la descentralización, buscamos que cada una de las personas entienda la importancia de la libertad financiera.  

A través de nuestras creencias y conocimientos, decidimos crear un espacio de asesorías para nuestra comunidad, con la mayor claridad y sencillez que permita comunicar nuestro mensaje a todos aquellos que quieran unirse a este movimiento de minería en español .
`,
        },
        {
          topic: '¿Qué es Bitcoin?',
          content: `Únete para aprender y estudiar sobre la moneda más fuerte del criptomercado, aprende a saber cómo invertir y vender en Bitcoin, sus ventajas, desventajas y tipos de wallets para guardar  tu dinero.`,
        },
        {
          topic: 'Mi primera operación de minería',
          content: `Podrás conocer en detalle cómo funcionan los equipos mineros, cómo configurar un pool minero, el mantenimiento preventivo y correctivo, también podrás conocer el cálculo de utilidades, el ROI y dónde se almacenan dichas utilidades.
`,
        },
        {
          topic: 'Firmware y Mining Pools',
          content: `En esta sección podrás obtener conocimiento sobre qué es un firmware y los diferentes tipos que existen, entre ellos están: BraiinsOs, HiveOs, entre otros. Aprenderás cómo cambiar el firmware de un minero y saber cuál es el indicado según el mining pool. También aprenderás qué es un Mining Pool, cómo funciona, haciendo énfasis en sus métodos de trabajo, sistema de recompensas, fee de retiro y más. 
`,
        },
        {
          topic: 'Marco regulatorio de minería en Venezuela',
          content: `Te asesoraremos sobre los documentos técnicos requeridos por la entidad regulatoria de la minería en el país (SUNACRIP) y el proveedor energético del país (CORPOELEC)  para garantizar la aprobación y emisión de la permisología respectiva que permite operar legalmente a la infraestructura de hashrate en Venezuela.
`,
        },
        {
          topic: 'Sistemas eléctricos para granjas de minería',
          content: `Te enseñaremos cómo obtener los cálculos necesarios para realizar la instalación eléctrica de una  infraestructura de hashrate, teniendo en cuenta el consumo eléctrico, la cantidad de equipos a instalar y el espacio geográfico; así como también, conocerás los riesgos asociados al no tomar en cuenta las debidas instrucciones y también tendrás la oportunidad de aprender cuáles son las zonas para ejercer la actividad minera en Venezuela. 
`,
        },
        {
          topic: 'Trading',
          content: `¿Gráficos a gran escala y zonas técnicas que no sabes interpretar? Confía e inicia con en nosotros y aprende sobre trading, analiza el mercado cripto y sus zonas potenciales para invertir.`,
        },
        {
          topic: 'Curso de reparación electrónica de ASIC´s',
          content: `Curso dedicado exclusivamente para aquellas personas que tengan conocimiento mínimo en electrónica, habilidad en el manejo de herramientas para la electrónica y conocimientos previos de soldadura. El curso se divide en dos niveles: hashboards y PSU.`,
        },
      ],
    },
    resources: {
      title: 'Recursos en Venezuela',
      text: 'Alineados con nuestro propósito, nos encontramos en permanente búsqueda de las más abundantes y eficientes riquezas naturales de nuestra región, para sembrarlas y transformarlas en crecimiento para nuestra sociedad.',
      resources: [
        {
          title: '1era Reserva de Petróleo',
          poster: '1era-reserva-de-petroleo.webp',
          video: '1era-reserva-de-petroleo',
        },
        {
          title: '5ta hidroeléctrica más grande',
          poster: '5ta-hidroelectrica.webp',
          video: '5ta-hidroelectrica',
        },
        {
          title: '8va reserva de gas',
          poster: '8va-reserva-de-gas.webp',
          video: '8va-reserva-de-gas',
        },
      ],
    },
    infrastructure: {
      title: 'Infraestructura',
      text: 'La infraestructura de hashrate es un espacio físico diseñado profesionalmente para hospedar equipos de minería de terceros o propios. La infraestructura es igual o incluso más importante que los mismos ASICs. Por esta razón, Doctorminer ha invertido todo su esfuerzo, tiempo y recursos en establecer los criterios de diseño más eficientes, basados en los más altos estándares internacionales, gracias a nuestra estrecha relación con los mineros más importantes de la industria a nivel global, así como en nuestro capital humano en las áreas de arquitectura, ingeniería y construcción con mayor experiencia en el sector local.',
      button: 'Descargar PDF',
    },
    hosting: {
      title: 'Hosting',
      text: 'El hospedaje es un servicio que consiste en instalar, supervisar, operar y mantener equipos de minería de terceros en infraestructuras de hashrate propias (granjas). Es ideal para aquellas personas o empresas, que deciden invertir en esta industria y no cuentan con los conocimientos técnicos, con capital suficiente o con una infraestructura adecuada para una operación de minería digital. Ofrecemos una experiencia placentera a través de una operación eficiente de infraestructuras de hashrate que nos permitan brindar un servicio óptimo y de calidad.',
      button: 'Descargar PDF',
    },
    hasportable: {
      title: 'Hasportable',
      text: `Las unidades móviles de minería son una solución común para la industria, que transforma containers convencionales en infraestructuras de hashrate, buscando aumentar la eficiencia, versatilidad y practicidad de la actividad. Doctorminer, apoyándose en su larga experiencia en diseño, ingeniería y construcción de infraestructuras de hashrate, desarrolló una solución propia llamada <span class="font-semibold text-primary">HASHPORTABLE</span>, que asegura la transportabilidad de la infraestructura según la disponibilidad energética en un sitio u otro, sin sacrificar su funcionamiento óptimo.`,
      button: 'Download PDF',
    },
    tribe: {
      text: `Doctorminer, en conjunto con una empresa americana, que se dedica al desarrollo tecnológico de la industria de la minería: Luxor, desarrolló el primer Mining Pool de Latinoamérica (TRIBU), el 30 de octubre del 2020. 

      TRIBU nació para contribuir con la seguridad de Bitcoin a través de la descentralización del poder de minado y el fortalecimiento de las industrias latinoamericanas de minería de Bitcoin aprovechando los recursos naturales que Latinoamérica ofrece, específicamente en Venezuela donde contamos con la 5ta hidroeléctrica más grande del mundo, somos la 1era reserva de petróleo y la 8va reserva de gas. 

      Para configurar  tu miner a nuestro pool debes asociarlo con el nombre de usuario con el que te registraste y utilizando nuestro stratum:`,
      button: 'Únete',
      statsText: ['PH/S', 'Mineros', 'Trabajadores', 'Eficiencia'],
    },
    asics: {
      buttons: 'Más información',
      descriptionParts: [
        'Equipo de minería digital, con una tasa de',
        'con el algoritmo de',
        'Consumo',
      ],
    },
    interviews: {
      title: 'Entrevistas',
      items: [
        {
          image: 'bic_artwork_mining_2.webp',
          date: 'Junio 26, 2021',
          category: 'artículos',
          title:
            '¿América Latina es una alternativa para los mineros de BTC tras la desconexión en China?',
          short:
            'En medio del problema de la desconexión de los mineros en China, la región pudiera presentarse como una opción.',
          link: 'https://es.beincrypto.com/america-latina-alternativa-mineros-bitcoin-btc-tras-desconexion-china/',
        },
        {
          image: 'canaan-mineria-venezuela-toukoumidis.webp',
          date: 'Noviembre 26, 2018',
          category: 'artículos',
          title:
            'La minería en Venezuela es prometedora según representante de Canaan',
          short:
            'La caída del precio de los criptoactivos ha tenido una incidencia significativa en la minería de criptomonedas, con la decisión de algunos mineros de apagar cierta cantidad de equipos debido a la reducción de su rentabilidad.',
          link: 'https://www.criptonoticias.com/entrevistas/mineria-venezuela-prometedora-canaan/',
        },
        {
          image: 'UCPZFO4NTRHUZOL2YEOM6UGP3I.webp',
          date: 'Agosto 2, 2021',
          category: 'artículos',
          title:
            'Banished Chinese Bitcoin Miners Look to the West, and Far Beyond',
          short:
            "One lesson Chinese miners have learned from the ban: Don't put all your eggs in one basket.",
          link: 'https://www.coindesk.com/business/2021/08/02/banished-chinese-bitcoin-miners-look-to-the-west-and-far-beyond/',
        },
        {
          image:
            '1434_aHR0cHM6Ly9zMy5jb2ludGVsZWdyYXBoLmNvbS9zdG9yYWdlL3VwbG9hZHMvdmlldy80NzdkOTA3ZDkyNjQyNDZmODg4ZTZmY2FkZTI2ZWE2Mi5qcGc=.webp',
          date: 'Septiembre 7, 2021',
          category: 'artículos',
          title:
            'BSL 2021: ¿Cuáles son los países de Latinoamérica donde conviene la minería de Bitcoin?',
          short:
            'Según Theodoro Toukoumidis, en Latinoamérica, el país más económico para minar es Venezuela. “La minería en Venezuela ha hecho muy bien. La generación de recursos que se inyectaron al país es visible”, aseveró. Y opinó que esta industria, a largo plazo, podría llegar a equipararse a la industria petrolera.',
          link: 'https://es.cointelegraph.com/news/which-latin-american-countries-are-suitable-for-bitcoin-mining',
        },
        {
          image: 'miner-theo.webp',
          date: 'Julio 9, 2021',
          category: 'artículos',
          title:
            'Venezuela, Argentina y Paraguay captarían parte del éxodo minero de China',
          short:
            'Las prohibiciones contra la minería de Bitcoin (BTC) en China y el éxodo de mineros digitales podría abrir oportunidades de negocios en Latinoamérica.',
          link: 'https://www.criptonoticias.com/entrevistas/venezuela-argentina-paraguay-captarian-parte-exodo-minero-china/',
        },
      ],
    },
    podcast: {
      title: 'Podcast',
      filters: {
        label: 'Tipo',
        placeholder: 'Seleccionar',
        button: 'Reiniciar',
        types: [
          {
            value: 'podcast',
            name: 'Podcast',
          },
          {
            value: 'criptonoticias',
            name: 'CriptoNoticias',
          },
          {
            value: 'mineriaDisruptiva',
            name: 'Minería Disruptiva',
          },
          {
            value: 'bitcointExperience',
            name: 'Bitcoin Experience',
          },
          {
            value: 'satoshiVenezuela',
            name: 'Satoshi Venezuela',
          },
          {
            value: 'viernesCriptografico',
            name: 'Gráfico',
          },
          {
            value: 'blockChainSummitLatam',
            name: 'itLatam',
          },
          {
            value: 'hashr8',
            name: 'HASH8',
          },
          {
            value: 'criptohispanos',
            name: 'Cripto Hispanos',
          },
          {
            value: 'swagSignal',
            name: 'Swag Signal',
          },
          {
            value: 'humanB',
            name: 'Human B',
          },
          {
            value: 'compassMining',
            name: 'Compass Mining',
          },
          {
            value: 'encriptados',
            name: 'Encriptados',
          },
        ],
      },
      items: [
        {
          image: 'podcast-zhMV9oXIhFk.webp',
          date: 'Junio 18, 2022',
          category: 'podcast',
          title: 'Las razones del crecimiento acelerado de la LATINOMINERÍA',
          short:
            'Hablamos con el CEO de Doctorminer sobre la minería de Bitcoin en Latinoamérica y su crecimiento en un especial Drinking Hashing donde cada bloque minado nos tomamos un fondo blanco.',
          link: 'https://www.youtube.com/watch?v=zhMV9oXIhFk',
        },
        {
          image: 'podcast-jC4V1M4YQlo.webp',
          date: 'Junio 1, 2021',
          category: 'podcast',
          title: '¿Cómo funciona un pool de minería de Bitcoin?',
          short:
            'Doctor Miner recientemente dio a conocer su pool de minería, que ya se encuentra en su segunda versión. Pero, ¿qué es un pool y cómo funciona? Recibimos a Theodoro Toukoumidis, CEO de la empresa para conocer junto a él todos los detalles.',
          link: 'https://www.youtube.com/watch?v=jC4V1M4YQlo',
        },
        {
          image: 'PJxlcxlZ71U.webp',
          date: 'Julio 9, 2021',
          category: 'criptonoticias',
          title: 'Entrevistando a Theodoro Toukoumidis de Doctorminer',
          short:
            'La minería de Bitcoin y criptomonedas es un mundo en constante evolución. Hoy les traemos una entrevista con Theodoro Toukoumidis, CEO de Doctorminer, empresa venezolana dedicada a la minería de BTC. Nuestro invitado ofreció sus reflexiones sobre el negocio de los pools de minería, la migración de mineros chinos y cuál es la actualidad de esta actividad en Venezuela. ',
          link: 'https://www.youtube.com/watch?v=PJxlcxlZ71U',
        },
        {
          image: '6-El-OzpKBk.webp',
          date: 'Abril 19, 2022',
          category: 'criptonoticias',
          title:
            '¿Cuál es el futuro de la minería en Venezuela? Fundador de Doctorminer nos da su opinión',
          short:
            'Nuestro corresponsal Luis Esparragoza conversó en esta oportunidad con Theo Toukoumidis desde la Bitcoin Conference 2022, para conocer los retos de la minería en Venezuela y cómo atraer inversion extranjera.',
          link: 'https://www.youtube.com/watch?v=6-El-OzpKBk',
        },
        {
          image: 'boYJ7Xmrq4w.webp',
          date: 'Noviembre 16, 2021',
          category: 'mineriaDisruptiva',
          title:
            'Cómo Bitcoin puede a ARREGLAR la Generación de Energía con Juan Jose Pinto',
          short:
            'Conoce más de Doctorminer: https://linktr.ee/Doctorminer | Sigue a Juan José: https://twitter.com/beachainbtc',
          link: 'https://www.youtube.com/watch?v=boYJ7Xmrq4w',
        },
        {
          image: 'zhMV9oXIhFk.webp',
          date: 'Enero 18, 2022',
          category: 'mineriaDisruptiva',
          title:
            'Las razones del crecimiento acelerado de la LATINOMINERÍA con Theo Toukoumidis de Doctorminer',
          short:
            'Hablamos con el CEO de Doctorminer sobre la minería de Bitcoin en Latinoamérica y su crecimiento en un especial Drinking Hashing donde cada bloque minado nos tomamos un fondo blanco.',
          link: 'https://www.youtube.com/watch?v=zhMV9oXIhFk',
        },
        {
          image: 'pfAzG3ty2_E.webp',
          date: 'Enero 28, 2022',
          category: 'bitcointExperience',
          title:
            '🦾Hablando de minería con Theodoro Toukoumidis, fundador de @doctorminer 🇻🇪👍🏻',
          short:
            '🚸 Breves palabras del fundador de @doctorminer, un reconocido emprendimiento🇻🇪 en el sector de la minería industrial con alto nivel de proyección internacional 🌎',
          link: 'https://www.youtube.com/watch?v=pfAzG3ty2_E',
        },
        {
          image: 'cYiTkyKfOoY.webp',
          date: 'Enero 19, 2021',
          category: 'satoshiVenezuela',
          title:
            'La magia de la minería de Bitcoin, ¿vale la pena? - Hablemos de Bitcoin con T. Toukoumidis',
          short:
            'La minería de Bitcoin es un tema que siempre llama la atención. Bien sea como oportunidad de inversión o por el consumo eléctrico, siempre hay alguien que nos pregunta cómo incursionar en este apasionante e interesante universo de hardwares y redes. ',
          link: 'https://www.youtube.com/watch?v=cYiTkyKfOoY',
        },
        {
          image: 'JKIoWSvfQNo.webp',
          date: 'Mayo 5, 2021',
          category: 'satoshiVenezuela',
          title:
            '⛏👨🏼‍🚀CONCEPTOS CLAVE A LA HORA DE MINAR BITCOIN en Venezuela y el mundo con Juan Pinto',
          short:
            'Minar Bitcoin es una actividad sumamente atractiva, especialmente por lo lucrativa que se ha vuelto. Sin embargo, para arrancar en este mundo es importante conocer elementos básicos sobre el tema, que te permitan decidir cuándo y cómo invertir para iniciar tu proyecto de minado.',
          link: 'https://www.youtube.com/watch?v=JKIoWSvfQNo',
        },
        {
          image: 'MUqyMukhje8.webp',
          date: 'Enero 27, 2021',
          category: 'satoshiVenezuela',
          title: 'Perspectivas 2021 para Venezuela - Hablemos de Bitcoin',
          short:
            'Para tener una visión más amplia de lo que se viene para Venezuela en este 2021, esta vez nos reunimos con 4 bitcoiners venezolanos. Estaremos con Alessandro Cecere, Guillermo Goncalvez, NICO y Juan Pinto para conocer sus opiniones sobre lo que sucede en Venezuela con respecto a Bitcoin, cómo se ve desde fuera y qué pasará en este 2021.',
          link: 'https://www.youtube.com/watch?v=MUqyMukhje8',
        },
        {
          image: 'jC4V1M4YQlo.webp',
          date: 'Enero 1, 2021',
          category: 'satoshiVenezuela',
          title:
            '¿Cómo funciona un pool de minería de Bitcoin? con Theodoro Toukoumidis',
          short:
            'Doctor Miner recientemente dio a conocer su pool de minería, que ya se encuentra en su segunda versión. Pero, ¿qué es un pool y cómo funciona? Recibimos a Theodoro Toukoumidis, CEO de la empresa para conocer junto a él todos los detalles.',
          link: 'https://www.youtube.com/watch?v=jC4V1M4YQlo',
        },
        {
          image: 'NBqcPRh9WPg.webp',
          date: 'Abril 30, 2021',
          category: 'viernesCriptografico',
          title: '🔥 APOCALIPSIS DE BITCOIN 🔥 #VC',
          short:
            '#ViernesCriptografico​ [T2-E17] ¡Llegó el fin de Bitcoin! ¡El apocalipsis ya está en el #ViernesCriptografico! Junto a tres  estudiosos y amplios conocedores del tema, evaluaremos cuales son las amenazas técnicas más tangibles que pudiera sufrir la red de la principal criptomoneda del mercado dentro de su código y funcionamiento.',
          link: 'https://www.youtube.com/watch?v=NBqcPRh9WPg',
        },
        {
          image: 'SdcZMY-FLaY.webp',
          date: 'Septiembre 11, 2020',
          category: 'viernesCriptografico',
          title: '#VC - ¿Es rentable minar Bitcoin en 2020?',
          short:
            'Bitcoin se encuentra sobre los $10k en comparación a los $3.8k de marzo, pero en mayo el protocolo madre sufrió su tercer halving. ¿Es rentable aun minarlo? ¿Sigue siendo Venezuela un país de referencia para la actividad de minado en LatAm? ¿Hay formas energéticas de optimizar esta actividad? Este Viernes Criptográfico responderá a todas estas preguntas.',
          link: 'https://www.youtube.com/watch?v=SdcZMY-FLaY',
        },
        {
          image: 'hA14oL_9FmE.webp',
          date: 'Agosto 26, 2021',
          category: 'blockChainSummitLatam',
          title: '#bslAnalisis [58]: Actualidad de la minería de Bitcoin',
          short:
            'Una nueva edición de tu programa favorito para analizar los temas que están siendo relevantes en el ecosistema Bitcoin, Criptomonedas, y Blockchain en Español.',
          link: 'https://www.youtube.com/watch?v=hA14oL_9FmE',
        },
        {
          image: '-_K5ThPfN2I.webp',
          date: 'Julio 17, 2021',
          category: 'blockChainSummitLatam',
          title:
            '#bslPodcast [56] con Theo Toukoumidis de Doctorminer sobre minería de Bitcoin en China, USA y LatAm',
          short:
            '👋 Hola, bienvenidos al Episodio N°56 del #bslPodcast. Nos acompaña Theodoro Toukoumidis, cofundador y CEO de Doctorminer, el primer pool de minería Bitcoin de Latinoamérica.',
          link: 'https://www.youtube.com/watch?v=-_K5ThPfN2I',
        },
        {
          image: 'zJRSBzD69uI.webp',
          date: 'Agosto 26, 2020',
          category: 'hashr8',
          title:
            'Minería de Bitcoin en Venezuela con Juan Pinto y Theo Toukoumidis',
          short:
            'En el episodio de hoy me acompañan Juan Pinto y Theo Toukoumidis de Doctor Miner y Hash Market. Fue genial conocer su opinión sobre la minería Bitcoin en Venezuela.  Escuchando su historia, hay algunas cosas salvajes que han sucedido en Venezuela. Hablan de cómo tuvieron que huir cuando se enteraron de que los mineros de Bitcoin en Venezuela estaban siendo extorsionados o sus equipos estaban siendo robados. Ambos básicamente se fueron a Europa por un año.  Y ahora están de vuelta, es legal minar Bitcoin en Venezuela y las criptomonedas se ven con mejores ojos que hace unos años..',
          link: 'https://www.youtube.com/watch?v=zJRSBzD69uI',
        },
        {
          image: 'X0ta-UZGfHg.webp',
          date: 'Marzo 27, 2021',
          category: 'criptohispanos',
          title:
            'Ep35 con Theodoro Toukoumidis de Doctorminer, sobre el rol del minero y minería en Latinoamérica',
          short:
            'Hola, comunidad criptohispana, bienvenidos al décimo episodio de la tercera temporada [E35 en total]. En esta oportunidad hablaremos de minería, algo de lo que conocemos en rigor como el proceso por el cual los computadores de una red blockchain validan transacciones y el protocolo emite y envía nuevas criptomonedas al computador (o los computadores) que lograron cerrar un bloque y todas las transacciones ahí dentro, pero la minería es mucho más que eso.',
          link: 'https://www.youtube.com/watch?v=X0ta-UZGfHg',
        },
        {
          image: 'inWvdzszpZI.webp',
          date: 'Junio 28, 2021',
          category: 'swagSignal',
          title: 'Minería, China y energía',
          short:
            'La minería de Bitcoin es una actividad sumamente importante para Bitcoin, pues brinda seguridad al registro de las transacciones que se realizan en la red desde 2009.',
          link: 'https://www.youtube.com/watch?v=inWvdzszpZI',
        },
        {
          image: 'RFSBWrAllzw.webp',
          date: 'Diciembre 21, 2021',
          category: 'humanB',
          title: 'Human B | El viaje a la madriguera de Bitcoin',
          short:
            'Ya existe en la Tierra una tecnología que cada vez adquiere más importancia. Todavía son pocos los que se han dado cuenta de su alcance.',
          link: 'https://www.youtube.com/watch?v=RFSBWrAllzw',
        },
        {
          image: 'gRgec_sET2o.webp',
          date: 'Agosto 29, 2021',
          category: 'compassMining',
          title: 'Minar Bitcoin en casa es común en América Latina',
          short:
            'Es habitual entrar en casas o comercios y ver una o varias máquinas mineras en marcha... o escucharlas. Dos mineros de distintos países de América Latina explican la minería a domicilio en la región',
          link: 'https://www.youtube.com/watch?v=gRgec_sET2o',
        },
        {
          image: 'zJRSBzD69uI.webp',
          date: 'Agosto 26, 2020',
          category: 'compassMining',
          title:
            'Minería de Bitcoin en Venezuela con Juan Pinto y Theo Toukoumidis',
          short:
            'En el episodio de hoy me acompañan Juan Pinto y Theo Toukoumidis de Doctor Miner & Hash Market para hablar de la minería de Bitcoin en Venezuela.',
          link: 'https://compassmining.io/education/mining-bitcoin-in-venezuela-with-juan-pinto-and-theo-toukoumidis/',
        },
        {
          image: 'DL0a2otrIHs.webp',
          date: 'Agosto 30, 2020',
          category: 'encriptados',
          title: '4to Encuentro Nacional de Mineros Digitales',
          short:
            '4to Encuentro de Mineros Digitales de la mano de SUNACRIP en videoconferencia con el Superintendente Joselit Ramirez por coyuntura actual de cuarentena.',
          link: 'https://www.youtube.com/watch?v=DL0a2otrIHs',
        },
      ],
    },
    home: {
      hero: {
        title: `Minar para <span class="text-primary">transformar</span>`,
        text: `<p class="text-primary">
        Somos un movimiento <br class="block md:hidden" />
        de minería en español
      </p>
      <p>
        que inspira a las personas a creer, aprender y minar Bitcoin para
        mejorar su vida.
      </p>`,
        buttons: [
          {
            hash: 'cree',
            name: 'Cree',
          },
          {
            hash: 'aprende',
            name: 'Aprende',
          },
          {
            hash: 'mina',
            name: 'Mina',
          },
        ],
      },
      believe: {
        title: 'Cree',
        text: 'Minar es una forma de creer. Es una actitud que asumimos para la vida. Somos creyentes porque sabemos que estamos participando en algo realmente grande, estamos abrazando una idea que lo cambió todo. Nos inspiran las ideas de libertad y Bitcoin es libertad en su máxima expresión.',
        button: {
          text: 'Cree',
          path: '/cree'
        }
      },
      learn: {
        title: 'Aprende',
        text: 'Te acompañamos a aprender sobre Bitcoin para que puedas cambiar tu vida a través de la minería. Queremos llevar adelante una comunidad basada en convicción con conocimiento, descubriendo y explorando en profundidad todo lo relacionado al universo de Bitcoin.',
        buttons: [
          {
            text: 'Asesorías',
            path: '/aprende/mentor'
          },
          {
            text: 'Entrevistas',
            path: '/aprende/entrevistas'
          },
          {
            text: 'Podcast',
            path: '/aprende/podcast'
          }
        ]
      },
      mine: {
        title: 'Mina',
        text: 'Somos una plataforma de ideas, servicios y recursos para el desarrollo eficiente y placentero de la minería. Minar es sembrar los recursos de Venezuela y Latinoamérica, contribuyendo a su desarrollo e impulsando el crecimiento y la libertad de las personas.',
        buttons: [
          {
            text: 'Productos',
            subitems: [
              {
                text: 'ASIC\'s',
                path: '/mina/productos/asics'
              },
              {
                text: 'Hasportable',
                path: '/mina/productos/hashportable'
              },
            ]
          },
          {
            text: 'Servicios',
            subitems: [
              {
                text: 'Tribu',
                path: '/mina/servicios/tribu'
              },
              {
                text: 'Hosting',
                path: '/mina/servicios/hosting'
              },
              {
                text: 'Infraestructura',
                path: '/mina/servicios/infraestructura'
              },
            ]
          },
          {
            text: 'Recursos',
            path: '/mina/recursos'
          }
        ]
      },
      us: {
        title: 'Nosotros',
        text: 'Somos una empresa de minería creyente de Bitcoin que potencia la economía financiera descentralizada desde Latinoamérica.',
        button: {
          text: 'Conócenos',
          path: '/nosotros'
        }
      }
    },
    errorPage: {
      videoSupportText: 'Su navegador no soporta la etiqueta de vídeo.',
      error404: {
        message: 'Esta página no pudo ser encontrada'
      },
      error500: {
        message: 'Hubo un error inesperado'
      },
      message: 'Por favor vuelva al inicio o póngase en contacto con nosotros.',
      ctas: [
        {
          text: 'Inicio',
          path: '/'
        },
        {
          text: 'Contacto',
          path: '/contacto'
        }
      ]
    }
  })
}
