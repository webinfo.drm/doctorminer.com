export default async (context, locale) => {
  return await Promise.resolve({
    fullMenu: [
      {
        name: 'Home',
        path: '/',
        type: 'internal', // internal, external, void, anchinternalor
        state: false,
        url: null,
        subItems: [],
      },
      {
        name: 'Believe',
        path: '/believe',
        type: 'internal',
        state: false,
        url: null,
        subItems: [],
      },
      {
        name: 'Learn',
        path: '/learn',
        type: 'void',
        state: false,
        url: null,
        subItems: [
          {
            name: 'Mentor',
            path: '/learn/consulting',
            type: 'internal',
            state: false,
            url: null,
            subItems: [],
          },
          {
            name: 'Entrevistas',
            path: '/learn/interviews',
            type: 'internal',
            state: false,
            url: null,
            subItems: [],
          },
          {
            name: 'Podcast',
            path: '/learn/podcast',
            type: 'internal',
            state: false,
            url: null,
            subItems: [],
          },
        ],
      },
      {
        name: 'Mine',
        path: '/mine',
        type: 'void',
        state: false,
        url: null,
        subItems: [
          {
            name: 'Productos',
            path: '/mine/products',
            type: 'void',
            state: false,
            url: null,
            subItems: [
              {
                name: "ASIC'S",
                path: '/mine/products/asics',
                type: 'internal',
                state: false,
                url: null,
                subItems: [],
              },
              {
                name: 'Hashportable',
                path: '/mine/products/hashportable',
                type: 'internal',
                state: false,
                url: null,
                subItems: [],
              },
            ],
          },
          {
            name: 'Services',
            path: '/mine/services',
            type: 'void',
            state: false,
            url: null,
            subItems: [
              {
                name: 'Tribu Pool',
                path: '/mine/services/tribe',
                type: 'internal',
                state: false,
                url: null,
                subItems: [],
              },
              {
                name: 'Hosting',
                path: '/mine/services/hosting',
                type: 'internal',
                state: false,
                url: null,
                subItems: [],
              },
              {
                name: 'Infraestructure',
                path: '/mine/services/infrastructure',
                type: 'internal',
                state: false,
                url: null,
                subItems: [],
              },
            ],
          },
          {
            name: 'Resources',
            path: '/mine/resources',
            type: 'internal',
            state: false,
            url: null,
            subItems: [],
          },
        ],
      },
      {
        name: 'FAQS',
        path: '/faqs',
        type: 'internal',
        state: false,
        url: null,
        subItems: [],
      },
      {
        name: 'Us',
        path: '/us',
        type: 'internal',
        state: false,
        url: null,
        subItems: [],
      },
      {
        name: 'Contact',
        path: '/contact',
        type: 'internal',
        state: false,
        url: null,
        subItems: [],
      },
    ],
    contact: {
      form: {
        fields: {
          name: 'Name',
          lastname: 'Lastname',
          email: 'Email',
          phone: 'Phone number',
          message: 'Message',
        },
        button: 'Send',
        title: 'Contact us',
        messages: {
          success:
            'Your email has been sent successfully, we will be contacting you soon. Thank you.',
          error:
            'Hubo un error al enviar su correo. Por favor intentelo más tarde o contáctenos vía WhatsApp.',
        },
      },
    },
    footer: {
      nav: {
        us: {
          title: 'About us',
          links: ['Storytelling', 'Staff'],
        },
        believe: {
          title: 'Believe',
          links: ['Purpose', 'Manifest'],
        },
        learn: {
          title: 'Learn',
          links: ['Mentor', 'Interviews', 'Podcast'],
        },
        mine: {
          title: 'Mine',
          links: [
            'Asics',
            'Tribu Pool',
            'Hosting',
            'Infrastructure',
            'Hashportables',
            'Resources',
          ],
        },
        follow: {
          title: 'Follow us',
        },
      },
      copyright: 'All rights reserved',
    },
    calculator: {
      title: 'Calculator',
      form: {
        fields: {
          hashrate: 'Hashrate',
          consumption: 'Energy consumption',
          opex: 'Operating costs (OPEX)',
          cost: 'Machine cost (USD)',
          hashprice: 'Hashprice',
        },
      },
      results: {
        title: 'Projections',
        fields: {
          'daily-income': 'Daily income',
          'daily-profit': 'Daily profit',
          'monthly-profit': 'Monthly profit',
          'roi-time': 'ROI time',
          'annual-roi': 'Annual ROI',
        },
        button: 'Share',
      },
    },
    believe: {
      title: 'Believe',
      purpose: {
        title: 'Purpose',
        text: 'Sow the resources of Latin America to contribute to its development and promote the growth and freedom of people.',
      },
      manifest: {
        title: 'Doctorminer Anthem',
        text: `Mining is a way of thinking.
        It is an attitude that we assume for life.
        It's a strong stance to participate in something big.
        To embrace an idea that changed everything.
        
        Mining is a way of believing.
        And we believe in decentralization.
        That's why we mine Bitcoin.
        Because Bitcoin has no gods or owners.
        
        Mining is a way of behaving
        is moving forward with purpose, vision and strategy.
        is utilizing our resources efficiently.
        To transform energy into value, 24/7.
        
        Mining is an opportunity for progress.
        Is a solution for the energy industry.
        Is an engine for growth in Latin America.
        Is a source of people’s freedom.
        
        Mining is a symbol of resistance.
        They’ll call us romantics for being passionate.
        They’ll call us weird for being disruptive.
        They’ll call us crazy, but crazy would actually be not doing it.`,
      },
    },
    us: {
      text: `We believe that bitcoin powers the decentralized financial economy from Latin America.

      We created a platform of ideas, services and resources for the efficient and pleasant development of mining and we invite you to our mining movement in Spanish that inspires people to believe, learn and mine Bitcoin to improve their lives.`,
      staff: {
        title: 'Staff',
        positions: [
          'CEO',
          'CBO',
          'COO',
          'CFO',
          'Sales Coordinator',
          'Operations Coordinator',
          'Technical Service Leader Barquisimeto',
          'Technical Service Leader Caracas',
          'Architect',
        ],
      },
    },
    faqs: {
      categoriesTitle: 'Categories',
      categories: ['sales', 'hosting', 'tribe', 'legal', 'others'],
      questions: {
        sales: [
          {
            question: 'Do you have mining machines for sale?',
            content: `Yes, you can contact us via whatsapp to our corporate number to obtain more information about the available models and our offers.`,
          },
          {
            question: 'What mining machines do you sell?',
            content: `We sell any generation, model and brand, used or new mining machines. If you are interested contact us via whatsapp.`,
          },
          {
            question: 'What PSU models do you have available?',
            content: `We offer all types of power sources for mining machines depending on our available inventory. If we do not have stock in inventory, we offer the possibility of making the request. If you want more information, write to us through our corporate number.`,
          },
          {
            question: 'Do you sell spare parts for mining machines?',
            content: `Yes, we have limited offers for different components of a great variety of models of mining machines. We offer the possibility of making the request. For more information write to us by Whatsapp Business.`,
          },
          {
            question: 'Do you sell mining machines at retail?',
            content: `Yes, the person must submit a contract with a national mining company that has the respective licenses and permits. However, Doctorminer offers the hosting service based on a certain amount of equipment, hashrate and consumption.`,
          },
          {
            question:
              'Do you have a trading license issued by the National Superintendency of Cryptoactives (SUNACRIP)?',
            content: `Yes, we have the commercialization license issued by the National Superintendency of Cryptoactives (SUNACRIP), this allows us to sell digital mining equipment nationwide.`,
          },
          {
            question:
              'How many days of guarantee do you offer for the purchase of a mining machine?',
            content: `The warranty days depend on the condition of the equipment (new or used), generation and model, however our standard is a 21-day warranty on used equipment.`,
          },
          {
            question: 'Sales contact',
            content: `
<a
  href="https://api.whatsapp.com/send?phone=582123241954"
  class="flex items-center justify-start font-semibold hover:text-primary hover:underline"
>
  <svg
    xmlns="http://www.w3.org/2000/svg"
    fill="currentColor"
    viewBox="0 0 24 24"
    class="w-5 h-5 mr-2"
  >
    <path
      fill-rule="evenodd"
      clip-rule="evenodd"
      d="M18.403 5.633A8.919 8.919 0 0 0 12.053 3c-4.948 0-8.976 4.027-8.978 8.977 0 1.582.413 3.126 1.198 4.488L3 21.116l4.759-1.249a8.981 8.981 0 0 0 4.29 1.093h.004c4.947 0 8.975-4.027 8.977-8.977a8.926 8.926 0 0 0-2.627-6.35m-6.35 13.812h-.003a7.446 7.446 0 0 1-3.798-1.041l-.272-.162-2.824.741.753-2.753-.177-.282a7.448 7.448 0 0 1-1.141-3.971c.002-4.114 3.349-7.461 7.465-7.461a7.413 7.413 0 0 1 5.275 2.188 7.42 7.42 0 0 1 2.183 5.279c-.002 4.114-3.349 7.462-7.461 7.462m4.093-5.589c-.225-.113-1.327-.655-1.533-.73-.205-.075-.354-.112-.504.112s-.58.729-.711.879-.262.168-.486.056-.947-.349-1.804-1.113c-.667-.595-1.117-1.329-1.248-1.554s-.014-.346.099-.458c.101-.1.224-.262.336-.393.112-.131.149-.224.224-.374s.038-.281-.019-.393c-.056-.113-.505-1.217-.692-1.666-.181-.435-.366-.377-.504-.383a9.65 9.65 0 0 0-.429-.008.826.826 0 0 0-.599.28c-.206.225-.785.767-.785 1.871s.804 2.171.916 2.321c.112.15 1.582 2.415 3.832 3.387.536.231.954.369 1.279.473.537.171 1.026.146 1.413.089.431-.064 1.327-.542 1.514-1.066.187-.524.187-.973.131-1.067-.056-.094-.207-.151-.43-.263"
    ></path>
  </svg>
  <span>+58 212-3241954</span>
</a>
            `,
          },
        ],
        hosting: [
          {
            question: 'Where do you provide the hosting service?',
            content: `Our company is based in Latin America, specifically in Venezuela. For security reasons we do not reveal the items of our hashrate infrastructures.`,
          },
          {
            question: 'What is the cost of the service?',
            content: `
<p>We have two business that revolve around the risk of volatility caused by the frequent fluctuation of network difficulty and the BTC price, which directly decreases the stability of the ASICs revenue generation:</p>
<ol class="leading-6 list-decimal list-inside list">
  <li><b>Fixed Cost</b>: the client absorbs all the <b>volatility risk</b>, by paying Doctorminer a stable amount corresponding to the electrical consumption of its mining equipment (KWh)</li>
  <li><b>Variable costse:</b> Where the client <b>mitigates part of the volatility risk</b>, by sharing with Doctorminer variable payments associated with % of income or profits generated by their equipment.</li>
</ol>
`,
          },
          {
            question: 'What are the benefits?',
            content: `
<ul class="leading-6 list-disc list-inside list">
  <li>Save money and time in the construction of the infrastructure.</li>
  <li>Saving time in the management of legal mining permits and energy feasibility</li>
  <li>Uptime greater than 85%.</li>
  <li>24/7 control and monitoring of your mining equipment.</li>
  <li>Personalized attention.</li>
</ul>
`,
          },
          {
            question: 'What does the service include?',
            content: `
<ul class="leading-6 list-disc list-inside list">
  <li>Connection of the equipment to the TRIBU mining pool.</li>
  <li>Technical service of your mining equipment (see file)</li>
  <li>All mining rewards will be deposited to the wallet designated by the client and at the end of the month a </li>notification of payment for the service provided (postpaid service) is sent.
  <li>Service contract in which the responsibilities of the client and Doctorminer are expressed.</li>
  <li>Remote access to a link to view the mining pool and the user's control panel.</li>
  <li>Control and monitoring software.</li>
  <li></li>Monthly reports.</li>
</ul>
`,
          },
          {
            question: 'What to do to hire the service?',
            content: `
<ul class="leading-6 list-disc list-inside list">
  <li>Notify quantity and model of machines to host.</li>
  <li>Check availability of energy charge and spaces.</li>
  <li>Request a business proposal from Doctorminer.</li>
  <li>Confirm geographic location of mining equipment.</li>
  <li>Sign service contract.</li>
</ul>
`,
          },
          {
            question: 'What machines can I host in the Doctorminer service?',
            content: `<p>We host computers with the Bitcoin SHA-256 algorithm. For example:</p>
<ul class="leading-6 list-disc list-inside list">
  <li>Antminer S9, S11, S15 and S19</li>
  <li>Antminer T15, T17, T19</li>
  <li>Antminer L3 and L7</li>
  <li>Antminer Z11 and Z15</li>
  <li>Whatsminer M20s, M21s, M30s and M31s</li>
  <li>Avalonminer 841, 1046, 1126 and 1166</li>
</ul>
`,
          },
          {
            question: 'Sales contact',
            content: `
<a
  href="https://api.whatsapp.com/send?phone=582123241954"
  class="flex items-center justify-start font-semibold hover:text-primary hover:underline"
>
  <svg
    xmlns="http://www.w3.org/2000/svg"
    fill="currentColor"
    viewBox="0 0 24 24"
    class="w-5 h-5 mr-2"
  >
    <path
      fill-rule="evenodd"
      clip-rule="evenodd"
      d="M18.403 5.633A8.919 8.919 0 0 0 12.053 3c-4.948 0-8.976 4.027-8.978 8.977 0 1.582.413 3.126 1.198 4.488L3 21.116l4.759-1.249a8.981 8.981 0 0 0 4.29 1.093h.004c4.947 0 8.975-4.027 8.977-8.977a8.926 8.926 0 0 0-2.627-6.35m-6.35 13.812h-.003a7.446 7.446 0 0 1-3.798-1.041l-.272-.162-2.824.741.753-2.753-.177-.282a7.448 7.448 0 0 1-1.141-3.971c.002-4.114 3.349-7.461 7.465-7.461a7.413 7.413 0 0 1 5.275 2.188 7.42 7.42 0 0 1 2.183 5.279c-.002 4.114-3.349 7.462-7.461 7.462m4.093-5.589c-.225-.113-1.327-.655-1.533-.73-.205-.075-.354-.112-.504.112s-.58.729-.711.879-.262.168-.486.056-.947-.349-1.804-1.113c-.667-.595-1.117-1.329-1.248-1.554s-.014-.346.099-.458c.101-.1.224-.262.336-.393.112-.131.149-.224.224-.374s.038-.281-.019-.393c-.056-.113-.505-1.217-.692-1.666-.181-.435-.366-.377-.504-.383a9.65 9.65 0 0 0-.429-.008.826.826 0 0 0-.599.28c-.206.225-.785.767-.785 1.871s.804 2.171.916 2.321c.112.15 1.582 2.415 3.832 3.387.536.231.954.369 1.279.473.537.171 1.026.146 1.413.089.431-.064 1.327-.542 1.514-1.066.187-.524.187-.973.131-1.067-.056-.094-.207-.151-.43-.263"
    ></path>
  </svg>
  <span>+58 212-3241954</span>
</a>
            `,
          },
        ],
        tribe: [
          {
            question: 'What is a mining pool?',
            content: `A mining pool is a technological platform that allows the integration of computing power created by various miners in order to increase network security, decentralization and financial efficiency of the activity.`,
          },
          {
            question: 'How much does my machine produce in Tribu?',
            content: `
<p>Depending on the mining power and the connectivity efficiency of your machine, in a month you will be able to really verify how much a mining machine produces in Tribu Pool or in any mining pool in the world. However, there are tools that help you project your income, based on your hashrate, the consumption of your mining equipment and electrical cost, the percentage you pay to the mining pool, and the efficiency of your mining operation.</p>
<a href="https://hashrateindex.com/tools/calculator" target="_blank">https://hashrateindex.com/tools/calculator</a>
`,
          },
          {
            question:
              'Where do I find the approximate amount of accumulated inTribu? What part/section of the pool is it in? Step by Step',
            content: `
<p>To know the accumulated that the Pool has, follow these steps:</p>
<ul class="leading-6 list-decimal list-inside list">
  <li>Log in with your pool account,</li>
  <li>Once the session has started, you must go to the menu that is located in the upper left side.</li>
  <li>In the menu you will look for the Mining Overview / Summary of Mining section.</li>
  <li>Once inside the Mining Overview / Resumen de mining section, two options will appear on the main panel.</li>
  <li>After having selected the POOL tab, there you will have the accumulated value of TRIBU Pool; as well as you will also have other types of information that can help you later.</li>
</ul>
<br>
<p>In the POOL tab, you can also find:</p>
<ul class="leading-6 list-disc list-inside list">
  <li>Average hashrate of the pool in the last hour,</li>
  <li>Average per PH (Petahash) per day,</li>
  <li>Pool Commission.</li>
</ul>
<br>
<p class="text-xs">In each of the lines, you can see the “i” of information, it has the legend of each one.</p>
`,
          },
          {
            question: 'What is the Tribe payment method?',
            content: `FPPS (FULL-PAY-PER-SHARE). This modality is the most beneficial for the miners, since the pool assumes all the risks of the variation in the block rewards (luck), this applies to both parts of the reward, both the one that comes from the commissions of the transactions and the new BTC created.`,
          },
          {
            question:
              'How much is the cost of the transfer FEE to my wallet? Does it transfer automatically?',
            content: `Our payment commissions tend to be zero, since we make a single payment for all the miners, that is the cost of the transaction divided among all the miners, one output for many inputs. The profits obtained are automatically deposited in the wallet, depending on the interval for the payments that have been configured in the pool.
            `,
          },
          {
            question:
              'What difference does mining with Tribu make than mining with another mining pool?',
            content: `
<ul class="leading-6 list-disc list-inside list">
  <li>Customer service in Spanish (local language).</li>
  <li>FPPS payment system that mitigates miners' risks</li>
  <li>Servers are geographically located in the same region, allowing low connection latency.</li>
  <li>User manual.</li>
  <li>Collection of commissions transparently.</li>
  <li>System of sub accounts for the same user.</li>
  <li>Flexible payout threshold, from 0.001 BTC to 0.5 BTC allows it to work for miners of all scales.</li>
  <li>Commissions for withdrawals that tend to be 0 when diluted among all TRIBU users.</li>
  <li>Configuration of alerts for hashrate fluctuations.</li>
</ul>
`,
          },
          {
            question: 'Can a team with BraiinsOS be configured to TRIBE?',
            content: `
<p>Yes, any equipment that mines with the SHA-256 algorithm can be configured. In any case, we invite you to watch the following video where we show you how to configure the equipment</p>
<br>
<a href="https://t.me/doctorminerVE/13720" target="_blank" class="btn btn-primary">Video</a>
`,
          },
          {
            question: "What is TRIBU's payment threshold?",
            content: `The payment threshold is flexible and you can set it to a value between 0.001 - 0.5 BTC. The withdrawal commission to your wallet tends to zero.`,
          },
          {
            question: 'How can I join TRIBE?',
            content: `
<ul class="leading-6 list-disc list-inside list">
  <li>Enter google in your search engine by placing <a href="https://mining.doctorminer.com/login" target="_blank">https://mining.doctorminer.com/login</a></li>
  <li>Once you are inside the page you must register with your preferred email and password (key with letters and numbers to increase security). You must confirm your password to finish your registration.</li>
  <li>Choose a username, you will receive an email to verify your account. In the event that the username is already registered, you should try a different one.</li>
  <li>When verifying your account, a new tab will open directly checking your email. In the following form enter your email and password.</li>
</ul>
            `,
          },
          {
            question: 'What is the stratum to set up the teams?',
            content: `To configure your miner to our pool you must associate it with the username with which you registered and using our stratum:  <b><a href="bitcoin.doctorminer.com:700" target="_blank">bitcoin.doctorminer.com:700</a></b>

<ul class="leading-6 list-disc list-inside list">
  <li>Set your Worker: Username/WorkerName</li>
  <li>Set your Password: x</li>
</ul>
<br>
The username can be an account or sub account.
<br>
For more information about TRIBU we invite you to join our Telegram channel: <a href="https://t.me/doctorminerpool" target="_blank">https://t.me/doctorminerpool</a>
`,
          },
          {
            question: 'What are the requirements to join the Tribu?',
            content: `Only by having mining equipment of the SHA-256 algorithm of the Bitcoin currency, you can enter our pool.`,
          },
          {
            question: 'What cryptocurrencies does Tribu operate?',
            content: `Bitcoin and Ethereum, Sha-256 algorithm and ETHas.`,
          },
          {
            question: 'What are the disadvantages?',
            content: `
<ul class="leading-6 list-disc list-inside list">
  <li>The new version takes a little longer to detect the miners when they first start to collect hashrates or when they change from an Active to a Warning or Inactive state.</li>
  <li>We do not have an app, however, we do have a mobile version that works very well.</li>
</ul>`,
          },
          {
            question: 'What wallet should I use?',
            content: `We recommend using non-custodial wallets or cold wallets. Tribu supports any type of valid Bitcoin wallet.`,
          },
          {
            question: 'Is the payment threshold set for each subaccount?',
            content: `Yes, once you register in TRIBU Pool, you can set up several subaccounts. In each of them you can configure different payment wallets and each configured payment threshold is independent of each subaccount.`,
          },
          {
            question:
              'What does the green, yellow and red signage mean in the "miners" section?',
            content: `In the following image we can see 3 icons. Which denote:
<ul class="leading-6 list-disc list-inside list">
  <li><b>✔  Green (active):</b> machines connected and mining.</li>
  <li><b>✔  Yellow (alerts):</b> this icon indicates the alarmed machines, which have presented a reboot or brief failure to connect to the pool.</li>
  <li><b>✔  Red (inactive):</b>  these are machines that have lost connection with the pool for about one hour.</li>
</ul>
`,
          },
          {
            question: 'What is the total  hashrate graph(global)?',
            content: `Through the following graph, we can see the performance of the machines in scales between 15 minutes to 24 hours.`,
          },
          {
            question: 'What is the summary table of the mining pool?',
            content: `The previous images show the summary table for the sub account that has been selected, in which we can see the hashrate and the total efficiency, as well as the mining income. Intervals can be selected between 15 minutes to 24 hours.`,
          },
          {
            question: 'Search by name.',
            content: `With the help of the following bar, you can search for a miner of your interest. We only have to write the name with which the equipment has been configured in the bar to start the search.`,
          },
          {
            question: 'Organization selection.',
            content: `Through these lists, you can establish a display order of the workers, depending on the selection made.`,
          },
          {
            question:
              'How do I join the referral program and what benefits do I get?',
            content: `If you want to be part of the referral program you must go to the last option in the menu that says "<b>referrals</b>".
<br>
To generate your link, click on "<b>referrals</b>", you must go down to the "<b>referral link list</b>" option.
<br>
Press the "<b>generate link</b>" option, place the referential code you want. Once created, you can copy the link and send it to the people you want to refer.
<br>
If you want to monitor the earnings obtained by your referrals, go to the section on the screen where it says "<b>daily earnings</b>".
<br>
The benefits that you can obtain are 10% of the fee of the miner that the person refers. (It is 10% of the 2.25% of the fee).
`,
          },
          {
            question: 'Can the income be divided into several portfolios?',
            content: `
To add your wallet to Tribu, you must go to the second section of the menu that says "<b>Sub Account Management</b>" and click on "<b>Wallet</b>".
<br>
Press the "<b>add wallet</b>" option, followed by "<b>create new wallet option</b>".
<br>
Then, you will have the following box "<b>create new BTC wallet address</b>". There you will place the name of your wallet and the address of it.
<br>
Once the data is entered, you must click on "<b>create wallet address</b>".
`,
          },
        ],
        legal: [
          {
            question:
              'What is the regulatory framework for the mining industry in Venezuela?',
            content: `
<b>GAZETTES:</b>
<div class="flex flex-col">
<div class="mt-5 w-full max-w-[320px] mx-auto lg:max-w-full overflow-x-auto shadow">
  <div class="inline-block min-w-full py-2 align-middle">
    <table id="gacetas-table" class="min-w-full border divide-y border-primary divide-primary">
    <thead class="bg-transparent border border-primary">
      <tr>
        <th scope="col" class="p-3 text-sm font-semibold text-left border text-primary border-primary">Normative Act</th>
        <th scope="col" class="p-3 text-sm font-semibold text-left border text-primary border-primary">Official Gazette No.</th>
        <th scope="col" class="p-3 text-sm font-semibold text-left border text-primary border-primary">Date</th>
        <th scope="col" class="p-3 text-sm font-semibold text-left border text-primary border-primary">Effective date</th>
        <th scope="col" class="p-3 text-sm font-semibold text-left border text-primary border-primary">Content</th>
        <th scope="col" class="p-3 text-sm font-semibold text-left border text-primary border-primary">Public Authority</th>
      </tr>
    </thead>
    <tbody class="bg-transparent divide-y divide-primary">
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decree n° 3.196</span></td>
        <td class="tablet-col">6.346 extraordinary</td>
        <td class="tablet-col">8 December 2017</td>
        <td class="tablet-col">Repealed</td>
        <td class="tablet-col">The creation of SUPCAVEN is authorized.</td>
        <td class="tablet-col">Presidency of the Republic</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decree n° 3.292</span></td>
        <td class="tablet-col">41.347</td>
        <td class="tablet-col">23 February 2018</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">It is determined as a support for the implementation of financial and commercial exchange operations through Cryptoassets.</td>
        <td class="tablet-col">Presidency of the Republic</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decree n° 3.333</span></td>
        <td class="tablet-col">41.366</td>
        <td class="tablet-col">22 March 2018</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">Creation of Petro Zones</td>
        <td class="tablet-col">Presidency of the Republic</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decree Constituyente sobre Criptoactivos y Moneda Soberana Petro</span></td>
        <td class="tablet-col">6.370 extraordinary</td>
        <td class="tablet-col">9 April 2018</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">Decree constituyente sobre criptoactivos y la Criptomoneda Soberana Petro</td>
        <td class="tablet-col">National Constituent Assembly</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decree n° 3.353</span></td>
        <td class="tablet-col">6.371 extraordinary</td>
        <td class="tablet-col">9 April 2018</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">Creation of Tesorería de Criptoactivos S.A.</td>
        <td class="tablet-col">Presidency of the Republic</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decree n° 3.355</span></td>
        <td class="tablet-col">6.371 extraordinary</td>
        <td class="tablet-col">9 April 2018</td>
        <td class="tablet-col">Repealed</td>
        <td class="tablet-col">Creation of SUPCACVEN</td>
        <td class="tablet-col">Presidency of the Republic</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decree n° 3.470</span></td>
        <td class="tablet-col">41.422</td>
        <td class="tablet-col">19 de June de 2018</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">SUPCAVEN is attached to the People's Ministry of Industries and National Production.</td>
        <td class="tablet-col">Presidency of the Republic</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decree n°. 3.719</span></td>
        <td class="tablet-col">6.420 extraordinary</td>
        <td class="tablet-col">28 December 2018</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">Taxpayers who carry out transactions in the national territory in foreign currency or cryptocurrencies, authorized by the Law, must determine and pay the obligations in foreign currency or cryptocurrencies.</td>
        <td class="tablet-col">Presidency of the Republic</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decree Constituyente sobre Sistema Integral de Criptoactivos</span></td>
        <td class="tablet-col">41.575</td>
        <td class="tablet-col">30 January 2019</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">Integral Crypto-assets System (name changed from SUPCAVEN to SUNACRIP)</td>
        <td class="tablet-col">National Constituent Assembly</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decree n° 4.025</span></td>
        <td class="tablet-col">41.763</td>
        <td class="tablet-col">19 November 2019</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">Individuals and legal entities, both public and private, are instructed as to the mandatory registration of information and economic facts expressed in Sovereign Cryptoassets, without prejudice to their registration in bolivars, as applicable.</td>
        <td class="tablet-col">Presidency of the Republic</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Providence 097-2019</span></td>
        <td class="tablet-col">41.782</td>
        <td class="tablet-col">16 December 2019</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">The parameters applicable to the allocation and exchange of crypto-asset resources through highly specialized technological platforms that guarantee the transparency, security and traceability of the operations are established.</td>
        <td class="tablet-col">SUNACRIP</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Providence 098-2019</span></td>
        <td class="tablet-col">41.787</td>
        <td class="tablet-col">26 December 2019</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">It establishes the parameters for the presentation of financial information, restatement of accounting records of transactions and economic events with Cryptoassets, carried out by individuals who are required to keep accounting records, as well as legal entities, public and private, in the territory of the Bolivarian Republic of Venezuela.</td>
        <td class="tablet-col">SUNACRIP</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decree n° 4.083</span></td>
        <td class="tablet-col">41.789</td>
        <td class="tablet-col">27 December 2019</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">The mining area number 2, called "Manuelita Sáenz", is created as Special Economic Zones of the Sovereign Cryptocurrency (Petro).</td>
        <td class="tablet-col">Presidency of the Republic</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Decree n°. 4.096 extraordinary</span></td>
        <td class="tablet-col">41.800</td>
        <td class="tablet-col">15 January 2020</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">Settlement, sale and payment of services in Petro Sovereign Crypto-assets (PTR)</td>
        <td class="tablet-col">Presidency of the Republic</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Providence 057-2020</span></td>
        <td class="tablet-col">41.955</td>
        <td class="tablet-col">1 September 2020</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">The requirements and procedures for sending and receiving remittances in Cryptoassets to individuals and legal entities in the territory of the Bolivarian Republic of Venezuela are established.</td>
        <td class="tablet-col">SUNACRIP</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Providence 084-2020</span></td>
        <td class="tablet-col">41.969</td>
        <td class="tablet-col">21 September 2020</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">It regulates the activities related to the use, importation, commercialization of Digital Mining equipment, parts and pieces thereof, equipment and conditioning of spaces to offer the hosting service to Digital Mining equipment, including the manufacture, assembly, repair and improvement of such equipment, as well as providing the digital mining service in the cloud.</td>
        <td class="tablet-col">SUNACRIP</td>
      </tr>
      <tr>
        <td class="tablet-col"><span class="text-base font-bold">Providence 044-2021</span></td>
        <td class="tablet-col">42.110</td>
        <td class="tablet-col">21 April 2021</td>
        <td class="tablet-col">Current</td>
        <td class="tablet-col">The Regulations related to the administration and control of risks related to money laundering, financing of terrorism and financing of the proliferation of weapons of mass destruction, applicable to virtual asset service providers and to persons and entities that provide products and services through activities involving virtual assets, in the Integral Cryptoassets System, are hereby enacted.</td>
        <td class="tablet-col">SUNACRIP</td>
      </tr>
    </tbody>
  </table>
  </div>
</div>
</div>

`,
          },
        ],
        others: [
          {
            question: 'What do you recommend to start mining?',
            content: `We invite you to start our consultancy "<b>my first mining operation</b>"`,
          },
        ],
      },
    },
    consulting: {
      title: 'Tópicos',
      topics: [
        {
          topic: 'Mentor',
          content: `We are inspired by the idea of running a business with a positive, and creative impact. We want to pass our Bitcoin and decentralization conviction. We seek that each person understands the importance of financial freedom.

          Through our beliefs and knowledge, we decided to create an advisory space for our community, with the greatest clarity and simplicity that allows us to communicate our message to all of those who want to join this Spanish mining movement.
`,
        },
        {
          topic: 'What is Bitcoin?',
          content: `A new form of money, decentralized and scarce. Join to learn why this development is game-changing for humanity.`,
        },
        {
          topic: 'My first mining operation',
          content: `You will be able to learn in detail how mining equipment works, how to configure a mining pool, preventive and corrective maintenance, you will also be able to know profit calculation, ROI and where said profit is stored.
`,
        },
        {
          topic: 'Firmware and Mining Pools',
          content: `In this section you will be able to obtain knowledge about the definition and types of firmware. You will learn how to change firmwares and know which one is indicated according to the mining pool. You will also learn what a Mining Pool is, how it works, emphasizing its working methods, reward system, withdrawal fee and more.
`,
        },
        {
          topic: 'Regulatory framework for mining in Venezuela',
          content: `We will advise you on the technical documents required by the mining regulatory entity in the country (SUNACRIP) and the country's energy provider (CORPOELEC) to make sure you get the approval and issuance of the respective permits that allow the hashrate infrastructure to legally operate in Venezuela.
`,
        },
        {
          topic: 'Electrical systems for mining farms',
          content: `We will teach you how to obtain the necessary engineering to carry out the electrical installation of a hashrate infrastructure, taking into account the electrical consumption, the amount of equipment to be installed and the geographical space; as well as let you know the risks associated with not taking into account the proper instructions and you will also have the opportunity to learn which are the areas to carry out mining activity in Venezuela.
`,
        },
        {
          topic: 'Trading',
          content: `Large-scale graphics and technical areas that you don't know how to read? Trust and start with us and learn about investing, analyze the crypto market and its potential areas to jump in.`,
        },
        {
          topic: "ASIC's electronic repair course",
          content: `It is a course dedicated exclusively to those people who have minimal knowledge of electronics, ability to use tools for electronics and previous knowledge of soldering. The course is divided into two levels: hashboards and PSU.`,
        },
      ],
    },
    resources: {
      title: 'Resources in Venezuela',
      text: 'Aligned with our purpose, we are constantly searching for the most abundant and efficient natural resources in our region, to sow them and transform them into growth for our society.',
      resources: [
        {
          title: '1st Oil Reserve',
          poster: '1era-reserva-de-petroleo.webp',
          video: '1era-reserva-de-petroleo',
        },
        {
          title: '5th largest hydroelectric plant',
          poster: '5ta-hidroelectrica.webp',
          video: '5ta-hidroelectrica',
        },
        {
          title: '8th largest gas reserve',
          poster: '8va-reserva-de-gas.webp',
          video: '8va-reserva-de-gas',
        },
      ],
    },
    infrastructure: {
      title: 'Infrastructure',
      text: 'The hashrate infrastructure is a professionally engineered physical space that hosts mining equipment. The infrastructure is as important as the ASICs themselves. For this reason, Doctorminer has invested a lot of effort, time and resources in establishing the most efficient design criteria, based on the highest international standards, due to our close relationship with the most important miners in the global industry, as well as in our human capital in the areas of architecture, engineering and construction with the most experience in the local sector.',
      button: 'Download PDF',
    },
    hosting: {
      title: 'Hosting',
      text: 'Hosting is a service that consists of installing, supervising, operating and maintaining third-party mining equipment in proprietary hashrate infrastructures (farms). It is ideal for those individuals or companies, who decide to invest in this industry and do not have the technical knowledge, sufficient capital or adequate infrastructure for a digital mining operation. Adequate infrastructure for a digital mining operation. We offer a pleasant experience through an efficient operation of hashrate infrastructures that allow us to provide an optimal and quality service.',
      button: 'Download PDF',
    },
    tribe: {
      text: `Doctorminer, together with an American company, which is dedicated to the technological development of the mining industry: Luxor, developed the first Mining Pool in Latin America (TRIBU), on October 30, 2020.

      TRIBU was born to contribute to the security of Bitcoin through the decentralization of mining power and the strengthening of the Latin American Bitcoin mining industries, taking advantage of the natural resources that Latin America offers, specifically in Venezuela where we have the 5th largest hydroelectric plant in the world, we are the 1st oil reserve and the 8th gas reserve.
      
      To configure your miner to our pool you must associate it with the username with which you registered and using our stratum:`,
      button: 'Join',
      statsText: ['PH/S', 'Miners', 'Workers', 'Efficiency'],
    },
    hasportable: {
      title: 'Hasportable',
      text: `Mobile mining units are a common solution for the industry, which transforms conventional containers into hashrate infrastructures, seeking to increase the efficiency, versatility and practicality of the activity. Doctorminer, relying on his vast experience in the design, engineering and construction of hashrate infrastructures, developed his own solution called <span class="font-semibold text-primary">HASHPORTABLE</span>, which ensures infrastructure transportability according to the energy availability in one place or another, without sacrificing its optimal operation.`,
      button: 'Download PDF',
    },
    asics: {
      buttons: 'More information',
      descriptionParts: [
        'Digital mining equipment, with a rate of',
        'with the algorithm of',
        'Consumption',
      ],
    },
    interviews: {
      title: 'Interviews',
      items: [
        {
          image: 'bic_artwork_mining_2.webp',
          date: 'June 26, 2021',
          category: 'articles',
          title:
            'Is Latin America an alternative for BTC miners after the shutdown in China?',
          short:
            'In the midst of the miners disconnection problem in China, the region could present itself as an option',
          link: 'https://es.beincrypto.com/america-latina-alternativa-mineros-bitcoin-btc-tras-desconexion-china/',
        },
        {
          image: 'canaan-mineria-venezuela-toukoumidis.webp',
          date: 'November 26, 2018',
          category: 'articles',
          title:
            'Mining in Venezuela is promising according to Canaan representative',
          short:
            'The fall in the price of cryptoassets has had a significant impact on cryptocurrency mining, with some miners deciding to shut down a certain amount of equipment due to reduced profitability.',
          link: 'https://www.CriptoNews.com/entrevistas/mineria-venezuela-prometedora-canaan/',
        },
        {
          image: 'UCPZFO4NTRHUZOL2YEOM6UGP3I.webp',
          date: 'August 2, 2021',
          category: 'articles',
          title:
            'Los mineros de Bitcoin chinos desterrados miran a Occidente y más allá',
          short:
            'Una lección que los mineros chinos han aprendido de la prohibición: No poner todos los huevos en la misma cesta.',
          link: 'https://www.coindesk.com/business/2021/08/02/banished-chinese-bitcoin-miners-look-to-the-west-and-far-beyond/',
        },
        {
          image:
            '1434_aHR0cHM6Ly9zMy5jb2ludGVsZWdyYXBoLmNvbS9zdG9yYWdlL3VwbG9hZHMvdmlldy80NzdkOTA3ZDkyNjQyNDZmODg4ZTZmY2FkZTI2ZWE2Mi5qcGc=.webp',
          date: 'September 7, 2021',
          category: 'articles',
          title:
            'BSL 2021: Which Latin American countries are suitable for Bitcoin mining?',
          short:
            'According to Theodoro Toukoumidis, in Latin America, the cheapest country to mine in is Venezuela. "Mining in Venezuela has done very well. The generation of resources injected into the country is visible," he said. And he said that this industry, in the long term, could reach the same level as the oil industry.',
          link: 'https://es.cointelegraph.com/news/which-latin-american-countries-are-suitable-for-bitcoin-mining',
        },
        {
          image: 'miner-theo.webp',
          date: 'July 9, 2021',
          category: 'articles',
          title:
            'Venezuela, Argentina and Paraguay could capture part of the mining exodus from China',
          short:
            'Bans against Bitcoin (BTC) mining in China and the exodus of digital miners could open up business opportunities in Latin America.',
          link: 'https://www.CriptoNews.com/entrevistas/venezuela-argentina-paraguay-captarian-parte-exodo-minero-china/',
        },
      ],
    },
    podcast: {
      title: 'Podcast',
      filters: {
        label: 'Type',
        placeholder: 'Select',
        button: 'Reset',
        types: [
          {
            value: 'podcast',
            name: 'Podcast',
          },
          {
            value: 'CriptoNews',
            name: 'CriptoNews',
          },
          {
            value: 'mineriaDisruptiva',
            name: 'Disruptive Mining',
          },
          {
            value: 'bitcointExperience',
            name: 'Bitcoin Experience',
          },
          {
            value: 'satoshiVenezuela',
            name: 'Satoshi Venezuela',
          },
          {
            value: 'viernesCriptografico',
            name: 'Graphic',
          },
          {
            value: 'blockChainSummitLatam',
            name: 'itLatam',
          },
          {
            value: 'hashr8',
            name: 'HASH8',
          },
          {
            value: 'criptohispanos',
            name: 'Cripto Hispanics',
          },
          {
            value: 'swagSignal',
            name: 'Swag Signal',
          },
          {
            value: 'humanB',
            name: 'Human B',
          },
          {
            value: 'compassMining',
            name: 'Compass Mining',
          },
          {
            value: 'encriptados',
            name: 'Encrypted',
          },
        ],
      },
      items: [
        {
          image: 'podcast-zhMV9oXIhFk.webp',
          date: 'June 18, 2022',
          category: 'podcast',
          title: 'The reasons for the accelerated growth of LATINOMINERIA',
          short:
            'We talked to Doctorminer\'s CEO about Bitcoin mining in Latin America and its growth in a special Drinking Hashing where each mined block we take a white background.',
          link: 'https://www.youtube.com/watch?v=zhMV9oXIhFk',
        },
        {
          image: 'podcast-jC4V1M4YQlo.webp',
          date: 'June 1, 2021',
          category: 'podcast',
          title: 'How does a Bitcoin mining pool work?',
          short:
            'Doctor Miner recently unveiled its mining pool, which is now in its second version. But what is a pool and how does it work? We welcomed Theodoro Toukoumidis, CEO of the company, to learn with him all the details.',
          link: 'https://www.youtube.com/watch?v=jC4V1M4YQlo',
        },
        {
          image: 'PJxlcxlZ71U.webp',
          date: 'July 9, 2021',
          category: 'CriptoNews',
          title: 'Interviewing Doctorminer\'s Theodoro Toukoumidis',
          short:
            'Bitcoin and cryptocurrency mining is a world in constant evolution. Today we bring you an interview with Theodoro Toukoumidis, CEO of Doctorminer, a Venezuelan company dedicated to BTC mining. Our guest offered his thoughts on the business of mining pools, the migration of Chinese miners and what is the current situation of this activity in Venezuela.',
          link: 'https://www.youtube.com/watch?v=PJxlcxlZ71U',
        },
        {
          image: '6-El-OzpKBk.webp',
          date: 'April 19, 2022',
          category: 'CriptoNews',
          title:
            'What is the future of mining in Venezuela? Founder of Doctorminer gives us his opinion',
          short:
            'Our correspondent Luis Esparragoza spoke with Theo Toukoumidis from the Bitcoin Conference 2022, to learn about the challenges of mining in Venezuela and how to attract foreign investment.',
          link: 'https://www.youtube.com/watch?v=6-El-OzpKBk',
        },
        {
          image: 'boYJ7Xmrq4w.webp',
          date: 'November 16, 2021',
          category: 'mineriaDisruptiva',
          title:
            'How Bitcoin can FIX Power Generation with Juan Jose Pinto?',
          short:
            'Learn more about Doctorminer: https://linktr.ee/Doctorminer | Follow Juan José: https://twitter.com/beachainbtc',
          link: 'https://www.youtube.com/watch?v=boYJ7Xmrq4w',
        },
        {
          image: 'zhMV9oXIhFk.webp',
          date: 'January 18, 2022',
          category: 'mineriaDisruptiva',
          title:
            'The reasons for the accelerated growth of LATINOMINERIA with Theo Toukoumidis of Doctorminer',
          short:
            'We talked to Doctorminer\'s CEO about Bitcoin mining in Latin America and its growth in a special Drinking Hashing where each mined block we take a white background.',
          link: 'https://www.youtube.com/watch?v=zhMV9oXIhFk',
        },
        {
          image: 'pfAzG3ty2_E.webp',
          date: 'January 28, 2022',
          category: 'bitcointExperience',
          title:
            '🦾Talking mining with Theodoro Toukoumidis, founder of @doctorminer. 🇻🇪👍🏻',
          short:
            '🚸 Brief words from the founder of @doctorminer, a recognized emprendimiento🇻🇪 in the industrial mining sector with a high level of international projection.🌎',
          link: 'https://www.youtube.com/watch?v=pfAzG3ty2_E',
        },
        {
          image: 'cYiTkyKfOoY.webp',
          date: 'January 19, 2021',
          category: 'satoshiVenezuela',
          title:
            'The magic of Bitcoin mining, is it worth it? - Let\'s talk about Bitcoin with T. Toukoumidisa',
          short:
            'Bitcoin mining is a topic that always attracts attention. Whether as an investment opportunity or for power consumption, there is always someone who asks us how to venture into this exciting and interesting universe of hardware and networks.',
          link: 'https://www.youtube.com/watch?v=cYiTkyKfOoY',
        },
        {
          image: 'JKIoWSvfQNo.webp',
          date: 'May 5, 2021',
          category: 'satoshiVenezuela',
          title:
            '⛏👨🏼‍🚀KEY CONCEPTS WHEN MINING BITCOIN in Venezuela and the World with Juan Pinto',
          short:
            'Mining Bitcoin is an extremely attractive activity, especially because of how lucrative it has become. However, to get started in this world it is important to know the basics about the subject, which will allow you to decide when and how to invest to start your mining project.',
          link: 'https://www.youtube.com/watch?v=JKIoWSvfQNo',
        },
        {
          image: 'MUqyMukhje8.webp',
          date: 'January 27, 2021',
          category: 'satoshiVenezuela',
          title: 'Outlook 2021 for Venezuela - Let\'s talk about Bitcoin',
          short:
            'To get a broader view of what is coming for Venezuela in 2021, this time we met with 4 Venezuelan bitcoiners. We will be with Alessandro Cecere, Guillermo Goncalvez, NICO and Juan Pinto to know their opinions about what is happening in Venezuela regarding Bitcoin, how it is seen from outside and what will happen in this 2021.',
          link: 'https://www.youtube.com/watch?v=MUqyMukhje8',
        },
        {
          image: 'jC4V1M4YQlo.webp',
          date: 'January 1, 2021',
          category: 'satoshiVenezuela',
          title:
            'How does a Bitcoin mining pool work? with Theodoro Toukoumidis',
          short:
            'Doctor Miner recently unveiled its mining pool, which is now in its second version. But what is a pool and how does it work? We welcomed Theodoro Toukoumidis, CEO of the company, to learn with him all the details.',
          link: 'https://www.youtube.com/watch?v=jC4V1M4YQlo',
        },
        {
          image: 'NBqcPRh9WPg.webp',
          date: 'April 30, 2021',
          category: 'viernesCriptografico',
          title: '🔥 BITCOIN APOCALYPSE 🔥 #VC',
          short:
            '#CryptoFriday [T2-E17] The end of Bitcoin is here! The apocalypse is already here on #CryptoFriday! Together with three scholars and experts on the subject, we will evaluate which are the most tangible technical threats that the network of the main cryptocurrency on the market could suffer within its code and operation.',
          link: 'https://www.youtube.com/watch?v=NBqcPRh9WPg',
        },
        {
          image: 'SdcZMY-FLaY.webp',
          date: 'September 11, 2020',
          category: 'viernesCriptografico',
          title: '#VC - Is it profitable to mine Bitcoin in 2020?',
          short:
            'Bitcoin is over $10k compared to $3.8k in March, but in May the mother protocol suffered its third halving. Is it still profitable to mine it? Is Venezuela still a reference country for mining activity in LatAm? Are there energetic ways to optimize this activity? This Crypto Friday will answer all these questions.',
          link: 'https://www.youtube.com/watch?v=SdcZMY-FLaY',
        },
        {
          image: 'hA14oL_9FmE.webp',
          date: 'August 26, 2021',
          category: 'blockChainSummitLatam',
          title: '#bslAnalisis [58]: Bitcoin mining news',
          short:
            'A new edition of your favorite program to analyze the topics that are becoming relevant in the Bitcoin, Cryptocurrency, and Blockchain ecosystem in Spanish.',
          link: 'https://www.youtube.com/watch?v=hA14oL_9FmE',
        },
        {
          image: '-_K5ThPfN2I.webp',
          date: 'July 17, 2021',
          category: 'blockChainSummitLatam',
          title:
          '#bslPodcast [56] with Doctorminer\'s Theo Toukoumidis on Bitcoin mining in China, USA and LatAm.',
          short:
            '👋 Hello, welcome to Episode #56 of the #bslPodcast. We are joined by Theodoro Toukoumidis, co-founder and CEO of Doctorminer, the first Bitcoin mining pool in Latin America.',
          link: 'https://www.youtube.com/watch?v=-_K5ThPfN2I',
        },
        {
          image: 'zJRSBzD69uI.webp',
          date: 'August 26, 2020',
          category: 'hashr8',
          title:
            'Mining Bitcoin In Venezuela with Juan Pinto and Theo Toukoumidis',
          short:
            "On today's episode I am joined by Juan Pinto and Theo Toukoumidis from Doctor Miner and Hash Market. It was great to get their take on Bitcoin mining in Venezuela.  Hearing their story, theirs some wild stuff going that's gone on in Venezuela. They talk about how they had to make a run for it when they found out that Bitcoin miners in Venezuela were being extorted or their equipment was being stolen. They both basically high-tailed it and went to Europe for a year.  And now they're back, it's legal to mine Bitcoin in Venezuela and crypto is looked upon much more favorably than it was just a few years ago.",
          link: 'https://www.youtube.com/watch?v=zJRSBzD69uI',
        },
        {
          image: 'X0ta-UZGfHg.webp',
          date: 'March 27, 2021',
          category: 'criptohispanos',
          title:
            'Ep35 with Theodoro Toukoumidis of Doctorminer, on the role of the miner and mining in Latin America',
          short:
            'Hello, cryptohispanic community, welcome to the tenth episode of the third season [E35 in total]. This time we will talk about mining, something we know strictly speaking as the process by which computers in a blockchain network validate transactions and the protocol issues and sends new cryptocurrencies to the computer (or computers) that managed to close a block and all transactions in there, but mining is much more than that.',
          link: 'https://www.youtube.com/watch?v=X0ta-UZGfHg',
        },
        {
          image: 'inWvdzszpZI.webp',
          date: 'June 28, 2021',
          category: 'swagSignal',
          title: 'Mining, China and energy',
          short:
            'Bitcoin mining is an extremely important activity for Bitcoin, as it provides security to the record of transactions made on the network since 2009.',
          link: 'https://www.youtube.com/watch?v=inWvdzszpZI',
        },
        {
          image: 'RFSBWrAllzw.webp',
          date: 'December 21, 2021',
          category: 'humanB',
          title: 'Human B | The Insight Journey Into The Bitcoin Rabbit Hole',
          short:
            'There already is a technology existing on earth which gains ever more importance. There are still few who have realized the extent.',
          link: 'https://www.youtube.com/watch?v=RFSBWrAllzw',
        },
        {
          image: 'gRgec_sET2o.webp',
          date: 'August 29, 2021',
          category: 'compassMining',
          title: 'Mining Bitcoin At Home Is Common In Latin America',
          short:
            "It's common to walk into homes or shops and see one or more mining machines running...or hear them. Two miners from different countries in Latin America explain at-home mining in the region.",
          link: 'https://www.youtube.com/watch?v=gRgec_sET2o',
        },
        {
          image: 'zJRSBzD69uI.webp',
          date: 'August 26, 2020',
          category: 'compassMining',
          title:
            'Mining Bitcoin In Venezuela with Juan Pinto and Theo Toukoumidis',
          short:
            "En el episodio de hoy me acompañan Juan Pinto y Theo Toukoumidis de Doctor Miner y Hash Market para hablar de la minería de Bitcoin en Venezuela..",
          link: 'https://compassmining.io/education/mining-bitcoin-in-venezuela-with-juan-pinto-and-theo-toukoumidis/',
        },
        {
          image: 'DL0a2otrIHs.webp',
          date: 'August 30, 2020',
          category: 'encriptados',
          title: '4th National Meeting of Digital Miners',
          short:
            '4th Meeting of Digital Miners with SUNACRIP in videoconference with Superintendent Joselit Ramirez due to the current quarantine situation.',
          link: 'https://www.youtube.com/watch?v=DL0a2otrIHs',
        },
      ],
    },
    home: {
      hero: {
        title: `Mine to <span class="text-primary">transform</span>`,
        text: `<p class="text-primary">
        We are a latinamerican <br class="block md:hidden" /> mining movement
      </p>
      <p>
      that inspires people to believe, learn and mine Bitcoin to improve their lives.
      </p>`,
        buttons: [
          {
            hash: 'cree',
            name: 'Believe',
          },
          {
            hash: 'aprende',
            name: 'Learn',
          },
          {
            hash: 'mina',
            name: 'Mine',
          },
        ],
      },
      believe: {
        title: 'Believe',
        text: ' Mining is a way of believing. It is an attitude we take on for life. We are believers because we know we are participating in something really big, we are embracing an idea that changed everything. We are inspired by ideas of freedom and Bitcoin is freedom at its best.',
        button: {
          text: 'Believe',
          path: '/believe'
        }
      },
      learn: {
        title: 'Learn',
        text: 'Come with us to learn about Bitcoin so you can change your life through mining. We want to run a community based on conviction with knowledge, discovering and exploring in depth everything related to the Bitcoin universe.',
        buttons: [
          {
            text: 'Mentor',
            path: '/learn/consulting'
          },
          {
            text: 'Interviews',
            path: '/learn/interviews'
          },
          {
            text: 'Podcast',
            path: '/learn/podcast'
          }
        ]
      },
      mine: {
        title: 'Mine',
        text: 'We are a platform of ideas, services and resources for the efficient and pleasant development of mining. Mining is to seed Venezuela’s and Latinamerica’s resources, to contribute with the development and to promote growth and freedom people, contributing to its development and promoting the growth and freedom of the people.',
        buttons: [
          {
            text: 'Products',
            subitems: [
              {
                text: 'ASIC\'s',
                path: '/mine/products/asics'
              },
              {
                text: 'Hasportable',
                path: '/mine/products/hashportable'
              },
            ]
          },
          {
            text: 'Services',
            subitems: [
              {
                text: 'Tribu Pool',
                path: '/mine/services/tribe'
              },
              {
                text: 'Hosting',
                path: '/mine/services/hosting'
              },
              {
                text: 'Infraestructura',
                path: '/mine/services/infrastructure'
              },
            ]
          },
          {
            text: 'Resources',
            path: '/mine/resources'
          }
        ]
      },
      us: {
        title: 'Us',
        text: 'We are a Bitcoin believer mining company that powers the decentralized financial economy from Latin America.',
        button: {
          text: 'Get to know us',
          path: '/us'
        }
      }
    },
    errorPage: {
      videoSupportText: 'Your browser does not support the video tag.',
      error404: {
        message: 'This page could not be found'
      },
      error500: {
        message: 'There was an unexpected error'
      },
      message: 'Please go back to top or contact us.',
      ctas: [
        {
          text: 'Home',
          path: '/'
        },
        {
          text: 'Contact',
          path: '/contact'
        }
      ]
    }
  })
}
