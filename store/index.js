export const state = () => ({
  calculatorState: false,
  asicsSelected: null
})

export const getters = {
  calculatorState (state) { return state.calculatorState },

  asicsSelected (state) { return state.calculatorState },
}

export const mutations = {
  SET_CALCULATOR_STATE (state, payload) {
    state.calculatorState = payload
  },

  SET_ASICS_SELECTION (state, payload) {
    state.calculatorState = payload
  },
}

export const actions = {
  setCalculatorState ({ commit }, payload) {
    commit('SET_CALCULATOR_STATE', payload)
  },

  setAsicsSelection ({ commit }, payload) {
    commit('SET_ASICS_SELECTION', payload)
  },
}