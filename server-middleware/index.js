/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
/* eslint-disable no-var */
import express from 'express'
import axios from 'axios'
const FormData = require('form-data')

const GSHEET_APP_URL_CONTACT =
  'https://script.google.com/macros/s/AKfycbyF3fUkLZh1WPWN1HKVu8_7zgSmhTLxozKJ2DGNKAO7PFyrmAA2Yz65VhmF8AEGUEHv/exec'

const app = express()
app.use(express.json())

app.post('/contact', async (req, res) => {
  const body = new FormData()
  Object.entries(req.body).forEach(([key, value]) => {
    body.append(key, value)
  })

  try {
    await axios.post(GSHEET_APP_URL_CONTACT, body)
      .then(function (response) {
        console.log(response.data, 'res')

        res.send(response.data)
      })
      .catch(function (error) {
        console.log(error, 'err')
      })
  } catch (error) {
    res.status(error.status).send(error)
  }
})

module.exports = app
