export default function ({ route, redirect }) {
    // Check if the current route is not the maintenance page
    if (route.path !== '/maintenance') {
      // Redirect to the maintenance page
      return redirect('/maintenance')
    }
  }